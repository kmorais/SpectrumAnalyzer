/*
    Copyright (C) <2019-2020> Kevin P. Morais <moraiskv@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_H
#define MAIN_H
/* -----------------------------------------
----------------------------------------- */

/* -----------------------------------------
    Useful MACROS
----------------------------------------- */
#define LOW(_data_)     (uint8_t)_data_
#define HIGH(_data_)    (_data_>>8)

#define ROUND(_data_,_th_)      \
if ( LOW(_data_) >= _th_ ) {    \
  _data_ += (1<<8);             \
}

// adc related
#define ADCChannel              (ADMUX & 0x07)
#define ADCSetChannel(__ch__)   (ADMUX = (ADMUX & 0xF8) | __ch__)

/* the correct would be to use movw at each
multiplication, but since it's considered
fixed-point 7:8, the upper byte (31:24) and
lower byte (7:0) will be ignored during
the shift, to set back to the desired
fixed-point (7:8 * 7:8 results in a 14:16
fixed-point number)

NOTE: from A0 to B0 = byte 0 to 1 of the first variable
A1 to B1 for the second variable, etc.
*/
#define mul_fp(_op1_,_op2_,_res_)   \
__asm__ __volatile__                \
(                                   \
  "muls   %B[_op1],%B[_op2]   \n\t \
  mov     %B[_res],r0   	    \n\t \
  mul     %A[_op1],%A[_op2]   \n\t \
  mov     %A[_res],r1         \n\t \
  mulsu   %B[_op2],%A[_op1]   \n\t \
  add     %A[_res],r0         \n\t \
  adc     %B[_res],r1         \n\t \
  mulsu   %B[_op1],%A[_op2]   \n\t \
  add     %A[_res],r0         \n\t \
  adc     %B[_res],r1         \n\t \
  clr     r1"                     \
  : [_res] "=&r" (_res_)          \
  : [_op1] "a" (_op1_)            \
  , [_op2] "a" (_op2_)            \
)

// note: it clear the higher byte of the input to use as 0
#define pow2_fp(_op_,_res_)         \
__asm__ __volatile__                \
(                                   \
  "mul    %B[_op],%B[_op]     \n\t \
  mov     %C[_res],r0   	    \n\t \
  mov     %D[_res],r1         \n\t \
  mul     %A[_op],%A[_op]     \n\t \
  mov     %A[_res],r0         \n\t \
  mov     %B[_res],r1         \n\t \
  mul     %B[_op],%A[_op]     \n\t \
  clr     %B[_op]             \n\t \
  add     %B[_res],r0         \n\t \
  adc     %C[_res],r1         \n\t \
  adc     %D[_res],%B[_op]    \n\t \
  add     %B[_res],r0         \n\t \
  adc     %C[_res],r1         \n\t \
  adc     %D[_res],%B[_op]"       \
  : [_res] "=r" (_res_)           \
  , [_op] "+r" (_op_)             \
)


/* -----------------------------------------
    FFT Info

dS         = number of points (samples) = 2^N
dSHalf     = half of input points, thus, it's the output length
dSOneEight = one eighth of S

*/
#define dS         128
#define dSHalf     dS/2
#define dSOneEight dS/8
#define dN         7

void FFTButterfly   (int16_t* pDataIn, int16_t* pDataOut);
void FFTExecute     (int16_t* pRealFFT, int16_t* pImagFFT);


/* -----------------------
LED Panel info

    dLedX       = number of led columns
    dLedY       = number of led lines
    dBandMaxSum = maximum distribution of the FFT result in to the LED panel
*/
#define dLedX 16
#define dLedY 16

#define dBandMaxSum    dSHalf/dLedX

/* -----------------------
A/D Converter info
NOTE: all definitions here are based in the fixed-point math used
in the code, 7:8, with 16 bits long data

    dADCMaxValue   = maximum adc value (2^10)
    dBandStep      = interval between ADC values that defien each value of band (from dBandMaxSum to 1)
    dBandTH        = minimum adc value which defines the use of the maximum band sum (dBandMaxSum)
*/
#define dADCMaxValue   1024

#define dBandStep  dADCMaxValue/dBandMaxSum
#define dBandTH    dADCMaxValue-dBandStep

/* -----------------------------------------
----------------------------------------- */
#endif
