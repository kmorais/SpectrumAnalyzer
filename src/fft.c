/*
    Copyright (C) <2019-2020> Kevin P. Morais <moraiskv@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* -----------------------------------------
    Include
----------------------------------------- */
// AVR headers
#include <avr/io.h>

// project headers
#include "main.h"

// pre created LUTs
// - gBufferflyLUT[S] = butterfly
// - TODO: exponential real part
// - TODO: exponential imag part
#include "lut_gen.h"

/* -----------------------------------------
    Test - Look-Up-Table
----------------------------------------- */
/**
 * Uncomment to test the FFT algorithm on a debugger with
 * a precalculated input signal. The variable is assigned
 * as 'const int16_t gTestDataLUT[S] = {...};
 * 
 *  Input Signal:
 *  - input samples created on octave to test the FFT (in fixed-point 7:8);
 *  -  f = 2kHz;     amplitude = 2.2 V;
 *  - gTestDataLUT = 2.2*sin(2*pi*2000 + 0).
 **/
// #define __TEST_FFT__ 1

#ifdef __TEST_FFT__
  #include "testlut_gen.h"
#endif

/* -----------------------------------------
    Butterfly Diagram
----------------------------------------- */
void FFTButterfly(int16_t* pDataIn , int16_t* pDataOut)  {
  uint8_t index;

  for ( uint8_t nAuxIndex = 0 ; nAuxIndex < dS ; nAuxIndex++ ) {
    index = gBufferflyLUT[nAuxIndex];

    #ifdef __TEST_FFT__
      *(pDataOut+nAuxIndex) = gTestDataLUT[index];
    #else
      *(pDataOut+nAuxIndex) = *(pDataIn+index);
    #endif
  }
}

/* -----------------------------------------
    FFT
----------------------------------------- */
void FFTExecute(int16_t* pRealFFT , int16_t* pImagFFT) {
  uint8_t index; // loop control
  uint8_t gain;  // gain group size (increase with 2^N)

  int16_t temp;  // temporary value

  /* ----------------------------------
  Stage = 1 ---> imaginary part equals to 0
  ---------------------------------- */
  gain = 1;

  for ( index = 0 ;  index < dS ; index += 2 ) {
    /* real */
    temp = *(pRealFFT+index) + *(pRealFFT+index+gain);;

    *(pRealFFT+index+gain) = *(pRealFFT+index) - *(pRealFFT+index+gain);

    *(pRealFFT+index) = temp;

    /* imaginary */
    *(pImagFFT+index)       = 0;
    *(pImagFFT+index+gain) = 0;
  }

  /* ----------------------------------
  Stage = 2 ---> gain = 1 for the first pair, and -1 for the second
      exp[i]  = exp( -(j*2*pi*i)/N )
      exp[0]  = lut_exp_r[0] + j*lut_exp_i[0] = 1 + j*0 = 1
      exp[S/4]= lut_exp_r[32] + j*lut_exp_i[32] = 0 + j*(-1) = -1*j
  ---------------------------------- */
  gain <<= 1;    // k = 2

  for ( index = 0 ; index < dS ; index += 4 ) {
    /* -- first pair => [0] and [2], for example -- */
    // real
    temp = *(pRealFFT+index) + *(pRealFFT+index+gain);

    *(pRealFFT+index+gain) = *(pRealFFT+index) - *(pRealFFT+index+gain);

    *(pRealFFT+index) = temp;

    // imaginary = 0

    /* -- second pair => [1] and [3], for example -- */
    // gain multiplication
    *(pImagFFT+index+1+gain) = -*(pRealFFT+index+1+gain);
    *(pRealFFT+index+1+gain) = 0;

    // real
    temp = *(pRealFFT+index+1) + *(pRealFFT+index+1+gain);

    *(pRealFFT+index+1+gain) = *(pRealFFT+index+1) - *(pRealFFT+index+1+gain);

    *(pRealFFT+index+1) = temp;

    // imaginary
    temp = *(pImagFFT+index+1) + *(pImagFFT+index+1+gain);

    *(pImagFFT+index+1+gain) = *(pImagFFT+index+1) - *(pImagFFT+index+1+gain);

    *(pImagFFT+index+1) = temp;
  }

  /* ----------------------------------
  Stage = 3 to N
  ---------------------------------- */
  int8_t nExponentialPositionStep = dSOneEight; // exponential LUT index decrement (at each stage)

  for ( uint8_t stage = 3; stage <= dN ; stage++) {  // stage loop
    // reset values, and increase k (size of gain group)
    gain <<= 1;

    uint8_t gainCount   = 0;  // count how many gains of a group are done
    uint8_t indexLUT = 0;     // hold the lut_exp index

    /* calculate N stage FFT */
    for ( index = 0 ; index < dS ; index++ ) {
      /* ------------------------------
      gain multiplication
          gain = (r_ptr + j*i_ptr) * (exp_r + j*exp_i)
      ------------------------------ */
      int16_t op1, op2;
      int16_t multResult;
      int16_t realGain, imagGain;  // real and imag part of the gain result

      // --- real part = (r_ptr * exp_r) - (i_ptr * exp_i)
      // r_ptr * exp_r
      op1 = *(pRealFFT+index+gain);
      op2 = gRealExpLUT[indexLUT];

      mul_fp(op1 , op2 , multResult);        // multiplication
      temp = multResult;

      // i_ptr * exp_i
      op1 = *(pImagFFT+index+gain);
      op2 = gImagExpLUT[indexLUT];

      mul_fp(op1 , op2 , multResult);        // multiplication
      realGain = temp - multResult;

      // --- imaginary part = (r_ptr * exp_i) + (i_ptr * exp_r)
      // r_ptr * exp_i
      op1 = *(pRealFFT+index+gain);
      op2 = gImagExpLUT[indexLUT];

      mul_fp(op1 , op2 , multResult);        // multiplication
      temp = multResult;

      // i_ptr * exp_r
      op1 = *(pImagFFT+index+gain);
      op2 = gRealExpLUT[indexLUT];

      mul_fp(op1 , op2 , multResult);        // multiplication
      imagGain = temp + multResult;

      /* ------------------------------
      Sum of points after gain multiplication
          here we divide the final result by 2 (shift) to avoid overflow;
          since at the FFT end it's done a division by 2^N = S, we just did it
          early, being needed only a division by 2^2 (stage 1 and 2 which
          are done outside this for loop)
      ------------------------------ */
      int16_t auxTemp;

      // real part
      temp    = *(pRealFFT+index) + realGain;
      auxTemp = *(pRealFFT+index) - realGain;

      *(pRealFFT+index)        = (temp >> 1);
      *(pRealFFT+index+gain)  = (auxTemp >> 1);

      // imaginary part
      temp    = *(pImagFFT+index) + imagGain;
      auxTemp = *(pImagFFT+index) - imagGain;

      *(pImagFFT+index)       = (temp >> 1);
      *(pImagFFT+index+gain) = (auxTemp >> 1);

      /* ------------------------------
      Gain group
          at each stage, the gain group (consecutive gain point) is
          multiplied by 2, being equal to k
      ------------------------------ */
      gainCount++;
      indexLUT += nExponentialPositionStep;
      if ( gainCount >=  gain ) {
        gainCount = 0;
        indexLUT  = 0;
        index     += gain;  // jumps over the gain group
      }
    }

    nExponentialPositionStep >>= 1;
  }

  /* ----------------------------------
  Module
      only the first 64 results matters, r_ptr[0:63]
  ---------------------------------- */
  for ( index = 0 ; index < dSHalf ; index++ ) {
    uint32_t realSquare;   // inputs extended do 32 bits
    uint32_t imagSquare;
    uint16_t real;         // inputs, real and imaginary part
    uint16_t imag;

    /* op1 = real part;    op2 = imaginary part */
    real = *(pRealFFT+index);
    imag = *(pImagFFT+index);

    /*
    does the remaining division (for stage 1 and 2 of the FFT),
    and get the absolute value: fft*(2/S) => fft*(2/4) => fft*(1/2)
    */
    real = (int16_t)real >> 1;
    imag = (int16_t)imag >> 1;

    if ( (int8_t)HIGH(real)  < 0 )
      real *= -1;
    if ( (int8_t)HIGH(imag)  < 0 )
      imag *= -1;

    /* calculate r^2 + i^2, with 32 bits result */
    pow2_fp( real , realSquare);
    pow2_fp( imag , imagSquare);

    uint32_t inputSquare = realSquare + imagSquare;

    /* extract the root, and store result */
    uint32_t bitMask       = 0x40000000;   // bit mask start at the MSb-1 (power of 4)
    uint32_t outputSquare  = 0;

    while ( bitMask > inputSquare )
      bitMask >>= 2;

    for ( ; bitMask > 0 ; bitMask >>= 2 ) {
      uint32_t nMaskOperation = bitMask + outputSquare;
      if ( inputSquare >= nMaskOperation ) {
        inputSquare  -= nMaskOperation;
        outputSquare = (outputSquare >> 1) + bitMask;
      }
      else
        outputSquare >>= 1;
    }

    /* square root input was 16:16 fixed-point, and the result as
    24:8 fixed-point, thus, there is no need to shift
    shift left 2 bits, to get a fixed-point result */
    *(pRealFFT+index) = (uint16_t)outputSquare;
  }
}
