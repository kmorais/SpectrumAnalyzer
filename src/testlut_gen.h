#ifndef _TEST_LUT_GEN_H_
#define _TEST_LUT_GEN_H_

const int16_t gTestDataLUT[128] = {
  0    , 498  , 464  , -64  , -525 , -424 , 129  , 545  , 379  , -191 , -557 , -328 ,
  251  , 563  , 273  , -307 , -560 , -215 , 360  , 550  , 153  , -407 , -533 , -90  ,
  449  , 509  , 25   , -485 , -478 , 39   , 515  , 441  , -104 , -538 , -397 , 167  ,
  553  , 349  , -228 , -561 , -295 , 286  , 562  , 238  , -340 , -555 , -178 , 389  ,
  541  , 115  , -433 , -519 , -50  , 472  , 491  , -14  , -504 , -456 , 79   , 530  ,
  415  , -142 , -548 , -368 , 204  , 559  , 317  , -263 , -563 , -261 , 319  , 559  ,
  202  , -370 , -547 , -140 , 417  , 529  , 76   , -458 , -503 , -11  , 492  , 470  ,
  -53  , -521 , -432 , 118  , 542  , 387  , -180 , -556 , -337 , 241  , 562  , 283  ,
  -298 , -561 , -225 , 351  , 553  , 164  , -399 , -537 , -101 , 442  , 514  , 36   ,
  -480 , -484 , 28   , 510  , 448  , -92  , -534 , -405 , 156  , 551  , 357  , -217 ,
  -561 , -305 , 276  , 562  , 248  , -331 , -557 , -188 
};

#endif
