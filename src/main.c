/*
    Copyright (C) <2019-2020> Kevin P. Morais <moraiskv@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* -----------------------------------------
    Include
----------------------------------------- */
// AVR headers
#include <avr/io.h>
#include <avr/interrupt.h>

/**This Macro is used to let the IDE/Code Editor parse data from the MCU
 * IO definitions, but must no be included at compile time. Thus, a
 * definition must be given (__m328p_inc__) to remove the header at
 * the compile command:
 *      Example: avr-gcc -g -Wall -D__m328p_inc__ main.c -o main
 * Some IDEs (like KDevelop) let you define special headers to be used
 * at parse time, removing the need of this logic. */
#ifndef __m328p_inc__
  #include <avr/iom328p.h>
#endif

// Project headers
#include "main.h"

/* -----------------------------------------
    Global Variables
----------------------------------------- */
/*
Led effects related
  gEffectType   = current effect mode, values range from 1 to 3
  gDelayFlag    = when 1, it means the delay effect is active
  gDelayT1Flag  = when 1, it means the timer 1 ISR has occurred
*/
uint8_t             gEffectType   = 3;
uint8_t             gDelayFlag    = 0;
volatile uint8_t    gDelayT1Flag  = 0;

/*
Led Panel Related
  gLedY             = all led column amplitude
  gLedYDelay        = all led column amplitude with delay
  gShiftRegister    = output coded to the shift reg (which is connected to the led panel)
*/
uint8_t gLedY[dLedX];
uint8_t gLedYDelay[dLedX];

uint16_t gShiftRegister[16];

#define SB_1    7   // pin used to transfer part higher part of the data (byte 15:8)
#define SB_0    4   // pin used to transfer the lower parte of the data (byte 7:0)
#define SB_RCLK 6   // pin used to control the serial register (both)
#define SB_SCLK 5   // pin used to control the serial clock (both)

/*
Data Related (buffer and FFT)
    gBufferIndex      = act as a pointer to the next position of the buffer
        to be filled, and also as a counter
    gBufferData       = buffer with S samples
    gRealFFT          = array with FFT real data
    gImagFFT          = array with FFT imaginary data
    gBand             = current band of the FFT to be used (max value of the ADC correspond to 100%)
*/
volatile uint8_t    gBufferIndex = 0;    // must be volatile (increment with ADC interrupts)
int16_t             gBufferData[dS];

int16_t gBand = dADCMaxValue;

#define ADC_MAIN_CHANNEL 0x05
#define ADC_BAND_CHANNEL 0x00

int16_t gRealFFT[dS];
int16_t gImagFFT[dS];

/* -----------------------------------------
    Functions
----------------------------------------- */
// setups functions that run only at the microcontroller start up
void firstSetup();     // initial configuration of the microcontroller

// others setup functions
void ledSetup();            // process the FFT output, to the led panel
void effectSetup();         // led panel effect setup
void delaySetup();          // set the state of delay effect
void shiftRegisterSetup();  // mask a value to be send into the shift register IC

/* -----------------------------------------
    Interrupt Services
----------------------------------------- */

/* --------------------
A/D Converter
  complete sample interrupt service routine
-------------------- */
ISR(ADC_vect) {
  /* ------------------------------
  -- BAND Channel
  ------------------------------ */
  /*  ADCChannelFlag =>
          2       = sample from main channel
          1       = switching channel (invalid data)
          others  = sample ignored (buffer filled)
          0       = sample from band channel
  */
  static uint8_t ADChannelFlag = 2;

  /* switching channel while a conversion is running, will result the next
  sample being from the previous channel */

  if ( gBufferIndex == dS )
  {
    // switch to band channel
    if ( ADCChannel == ADC_MAIN_CHANNEL )
      ADCSetChannel( ADC_BAND_CHANNEL );
    // read band channel and switch back to main
    else if ( ADChannelFlag == 0 ) {
      gBand = ADC;
      ADCSetChannel( ADC_MAIN_CHANNEL );

      ADChannelFlag = 1;
      gBufferIndex++;

      return;
    }

    ADChannelFlag--;
    return;
  }
  else if ( gBufferIndex > dS || ADChannelFlag == 1) {
    ADChannelFlag = 2;
    return;
  }

  /* ------------------------------
  -- MAIN Channel
  ------------------------------ */
  int16_t    sampleRaw;
  int16_t    sampleDone;

  /*
  the entire code math works with fixed-point 6:8, except here,
    which uses 6:10 (the MSb is used also) to provide the maximum precision
    from the sample
    -> voltageResolution   = 5 V / 1024 = 4,88 mV = 0x0005 as fixed-point 6:10
    -> voltageOffset       = 1.5 V (as fixed-point 6:10), used to remove analog offset (which
    are inserted to read sample negative values), thus, the maximum voltage desired is 3 V
  */
  uint8_t     voltageResolution  = 0x05;
  uint16_t    voltageOffset      = (3 << 9);

  /* read the sample and process it to the fixed-point in use */
  sampleRaw = ADC;

  sampleRaw *= voltageResolution;
  sampleRaw -= voltageOffset;

  /* NOTE: it's needed a arithmetic shift here, not logical */
  sampleDone = (sampleRaw >> 2);    // set from 6:10 to 6:8 fixed-point

  /* store the new sample in fixed-point to the buffer */
  gBufferData[gBufferIndex] = sampleDone;
  gBufferIndex++;
}


/* --------------------
Timer 1 Compare to reg.A interrupt service routine
    used to create the fall delay in the led panel output
Timer 0 Compare to reg.B interrupt service routine
    used to multiplex each column of the led panel
-------------------- */
ISR(TIMER1_COMPA_vect) {
  gDelayT1Flag = 1;
  TCNT1 = 0x0000;     // reset counter
}

ISR(TIMER0_COMPB_vect) {
  /*
  load the current column index at PB3:0, and increment it
  to the next position
  */
  uint8_t  ledX;
  ledX = PORTB + 1;
  ledX &= 0x0F;      // work as ==> if (nLedX >= n_led_C) { nLedX=0;} (overflow),
                      // and also removes pull up R value at PB7:4

  /*
  PORTD7:4 used to control shift registers (two in total, for 16 bits data)
      SB_1    = serial data of the higher byte
      SB_0    = serial data of the lower byte
      SB_SCLK = clock input for the serial shift register (both of then)
      SB_RCLK = clock input for the output shift register (both of then)
  NOTE: shift register CI timing (for 5 V): SN74HC595
      minimum clock pulse (H or L, for PD6:4) = 20 ns
      serial input setup time (before clk rising edge) = 25 ns
  NOTE: microcontroller CPU clock = 20 MHz => 50 ns period
  */

  /*
  serial transfer of the 1st and 2st byte in to the shift registers
  note: separated for each byte, always using the MSb
  */
  uint16_t  shift   = gShiftRegister[ledX];
  uint8_t   shiftH  = HIGH(shift);
  uint8_t   shiftL  = LOW(shift);

  PORTD &= 0x0F;  // put all connections with the shift register IC to 0

  for ( int8_t i = 7 ; i >= 0 ; i-- ) {
    // higher byte
    if ( ( shiftH & (1 << 7) ) != 0 )
      PORTD |= (1 << SB_1);
    else
      PORTD &= ~(1 << SB_1);

    // lower byte
    if ( ( shiftL & (1 << 7) ) != 0 )
      PORTD |= (1 << SB_0);
    else
      PORTD &= ~(1 << SB_0);

    // create a clock pulse to send the bit, while moving the
    // next bit to the MSb position
    PORTD |= (1 << SB_SCLK);

    shiftH <<= 1;
    shiftL <<= 1;

    PORTD &=  ~(1 << SB_SCLK);
  }

  /* finish transfer by creating a pulse at the output register of the IC */
  PORTD |= (1 << SB_RCLK);
  PORTD &= ~(1 << SB_RCLK);

  /* set the column index value at PORTB3:0 (leaving the pull up R active for 5:4) */
  ledX |= 0x30;
  PORTB = ledX;
}

/* -----------------------------------------
    Main
----------------------------------------- */
int main() {
  /* -------
  Data pointers
  ------- */
  int16_t* pBuffer;
  int16_t* pRealFFT;
  int16_t* pImagFFT;

  pBuffer   = &gBufferData[0];
  pRealFFT  = &gRealFFT[0];
  pImagFFT  = &gImagFFT[0];

  /* -------
  Setup calls
  ------- */
  firstSetup();

  ledSetup();
  effectSetup();
  delaySetup();
  shiftRegisterSetup();

  // global interrupt enable flag
  sei();

  // -----------------------
  // inf loop
  while (1) {
    /*
    check if the data buffer is filled with samples,
        so we can run the FFT
        NOTE: must be higher than S (buffer size) since there is an extra
        sample from the band channel, thus, ' > S', and not ' >= S' */
    if ( gBufferIndex > dS ) {
      /* transfer samples from the data buffer and reset it's counter
      note: the data is shuffled according to the butterfly diagram */
      FFTButterfly(pBuffer , pRealFFT);
      gBufferIndex = 0;

      FFTExecute(pRealFFT , pImagFFT);

      /* redefine the led matrix value, and
      update the current effect and delay */
      ledSetup();
      effectSetup();
      delaySetup();
      shiftRegisterSetup();
    }

    /* check if the delay t1 flag is active */
    if ( gDelayT1Flag == 1 ) {
      for (uint8_t index = 0; index < dLedX ; index++) {
        if (gLedYDelay[index] > 0)
          gLedYDelay[index]--;
      }
      gDelayT1Flag = 0;
    }

  }
}

/* -----------------------------------------
    Setup - one time run
----------------------------------------- */
void firstSetup() {
  /* ----------------
  ports setup:
      PORTB =>
                  PB7:6 = external clock (crystal oscillator)
                  PB3:0 = X column index, value range from 0 to 15
      PORTC =>
                  PC6 = reset pin, connected to a push button, which is connected to GND
                  PC5 = A/D converter channel
                  PC4 = effect 2, active at '0'
                  PC3 = effect 1, active at '0'
                  PC2 = delay button/led, when on, it means the delay is active
      PORTD =>
                  PD7 = shift register connection, serial bit transfer
                  PD6 = shift register connection, clock input (for both storage shift register 1 and 2)
                  PD5 = shift register connection, clock input for the shift register 2 (ctrl led panel Y lines 8 to 15)
                  PD4 = shift register connection, clock input for the shift register 1 (ctrl led panel Y lines 0 to 7)
  unused ports:
      PORTB =>
                  PB5:4 = RESERVED (can be used to expand the number of columns)
      PORTC =>
                  PC2|0
      PORTD =>
                  PD3:0
  */
  DDRB    = 0x0F;     // PB7:4 = in ; PB3:0 = out0
  PORTB   = 0x30;     // active the pull-up R for PB5:4

  DDRC    = 0x00;     // PC6:0 = in
  PORTC   = 0x1C;     // active the pull-up R for PC4:2

  DDRD    = 0xF0;     // PD3:0 = in ; PD7:4 = out
  PORTD   = 0x0F;     // active the pull-up R for PD3:0

  /*  ----------------
  timer 1 setup:
      used to create a falling delay to the output led panel (which uses the FFT result)
  */
  TCCR1A  = 0x00;     // configuration register

  OCR1A   = 0x0989;   // compare reg. A, ~125ms with the choosen clk (prescaler = 1024)
  TCNT1   = 0x0000;   // timer 1 start value

  /*  ----------------
  timer 0 setup:
      100 Hz refresh rate (10ms), which is divided between all columns (n_led_x)
  */
  TCCR0A  = 0x00;     // normal mode, WGM01 = WGM00 = 0
  TCCR0B  = 0x05;     // CS02 = CS01 = CS00 = 1, clock prescaling = 1024, WGM02 = 0

  OCR0B   = 12;       // compare reg. B
  TCNT0   = 0x00;     // timer 0 start value

  TIMSK0  = 0x04;     // enable compare to reg. B interrupts

  /* --------------------
  A/D converter setup */
  DIDR0   = 0xE3;     // disable PC5|1:0 logic ports
  ADMUX   = 0x40;     // Vref = AVcc = 5V ; A/D converter connected to PC5
  ADMUX   |= ADC_MAIN_CHANNEL;

  ADCSRA  = 0xAF;     // clock prescaling = 128, auto-triggering mode
  ADCSRB  = 0x00;     // free running mode

  ADCSRA  |= 0x40;    // set the bit ADSC, starting the converter
}

/* -----------------------------------------
    Setup while running
----------------------------------------- */
void effectSetup() {
  /*
  Effects:
    effect 1: only the higher led will be set in a column
    effect 2: only the 3 higher led's will be set in a column
    effect 3: all bits from the lower to the top will be set in a column
  */
  uint8_t PB;

  PB = ~(PINC | 0xE7);   // leave only PC4 and 3

  if ( PB == 0x08 )      // effect 1
    gEffectType = 0;
  else if ( PB == 0x10 ) // effect 2
    gEffectType = 2;
  else                    // effect 3
    gEffectType = 15;
}

/* --------------------
-------------------- */
void delaySetup() {
  uint8_t PCMask;
  PCMask = ~(PINC | 0xFB);    // leave only PC2

  if ( PCMask == 0x04 ) {     // delay on
    // turn on the flag delay and enable T1 comp. reg. A interrupt
    gDelayFlag = 1;

    TCCR1B = 0x05;      // prescaling = 1024
    TIMSK1 = 0x02;
  }
  else {                   // delay off
    // turn off the delay flag and disabled T1 comp. reg. A interrupt
    gDelayFlag = 0;

    TCCR1B  = 0x00;
    TIMSK1  = 0x00;
    TCNT1   = 0x0000;
  }
}

/* --------------------
-------------------- */
void ledSetup() {
  /*
  change ADC channel from MAIN to BAND:
      BAND is used to define the frequency band (from the FFT result)
      to be used
  */
  uint8_t bandSum = 0;

  for ( ; gBand > 0 ; gBand -= 256 )
    bandSum++;

  if ( bandSum == 0 )
    bandSum++;

  /* sum of the FFT results to be distributed along the LED panel
      scaleX     =  scaling factor to put the result between 0 and 15 (according to the number of
          led columns)
      roundValue = 0x81;  0,50390625 in 7:8 fixed-point, used to round results
  */
  uint8_t scaleX     = 10;
  uint8_t roundValue = 0x81;

  uint8_t index;         // loop control

  int16_t* pRealFFT;    // pointer to fft result
  pRealFFT = &gRealFFT[0];

  for ( index = 0; index < dLedX; index++) {
    uint8_t  sum = 0;

    uint16_t multResult;

    /* column loop */
    for ( uint8_t auxIndex = 1 ; auxIndex <= bandSum ; auxIndex++ ) {
      // multiply by the constant, and do a round check (used 0.5)
      multResult = *(pRealFFT) * scaleX;
      ROUND(multResult , roundValue);

      // sum and pointer increment
      sum += HIGH(multResult);
      pRealFFT++;
    }

    /* store the column value */
    if ( sum >= dLedY )
      sum = dLedY-1;

    gLedY[index] = sum;
  }
}


/* -----------------------------------------
    Shift Register Coding
----------------------------------------- */
void shiftRegisterSetup() {
  uint8_t index;     // loop control
  uint8_t bitIndex;  // bit position to be set in the shift register

  uint16_t mask;     // temporary value

  for ( index = 0 ; index < dLedX ; index++ ) {
    // get the higher bit position to be set in the shift register
    bitIndex = gLedY[index] - gEffectType;

    if ( bitIndex >= dLedX )
      bitIndex = 0;

    mask = 0x0000;
    for ( ; bitIndex <= gLedY[index] ; bitIndex++)
      mask |= (1 << bitIndex);

    /*
    check if the delay column is lower than the actual column, and
    set the delay line (which should be >=), if the delay effect
    is enabled
    */
    if ( gDelayFlag == 1 ) {
      if ( gLedY[index] > gLedYDelay[index] )
        gLedYDelay[index] = bitIndex - 1;
      mask |= (1 << gLedYDelay[index]);
    }

    // output
    gShiftRegister[index] = mask;
  }
}
