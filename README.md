# Spectrum Analyzer - ATMEGA328p

Software written in C, which process an input signal (provided by a A/D converter), by using the **_FFT - Fast Fourier Transform_**.


<!-- ----------- -->
## Schematics/PCB Layout

The schematics/pcb layout, which were created using the software KiCad 5.1, can be found under the directory **_pcb_**. The board design was divided in two, one for most of the system (which process the FFT through a microcontroller) and the other to power a LED panel, working a current source.


<!-- ----------- -->
## ATMEGA328p

The target device for the code is the microcontroller **_ATMEGA328p_**, developed by [Atmel](https://www.microchip.com/wwwproducts/en/ATmega328p).
It's CPU works with 8 bits data, running 16 bits long instructions. The main features to be said here are:

- CPU up to 20 MHz
- SRAM of 2,048 KB
- Program Memory of 32 KB
- A/D Converter with 10-bits precision, from 0 to 5 V
- Timers: 2x 8 bits, and 1x 16 bits


<!-- ----------- -->
## Input Signal

The input signal to be sampled by the microcontroller A/D converter, and processed through the FFT, is an audio signal. From the 0 to 5 V of the microcontroller ADC voltage range, it's used 0 to 3 V. Measuring then -1.5 V to +1.5 V, which will pass through an analogic offset of 1.5 V, removed later through software.


<!-- ----------- -->
## Fixed-Point Math

Due to the small size of data from the microcontroller register (8 bits), it's was decided to use fixed-point math 7:8, thus, having two register for each data. The mathematic language Octave was chosen to simulate some of the algorithms, including the creation of Look-Up-Tables. The LUT's stored in the software as constants (to speed-up runtime) are the butterfly recombination (it's index), and both the exponential (real and imaginary part) of the FFT equation. All the Octave simulations are under the **_octave_** directory.


<!-- ----------- -->
## FFT - Fast Fourier Transform

The code runs a 128 points FFT, thus, giving 64 outputs. According to the operation frequency of the A/D converter, the range is up to **_11.574 kHz_** (with full resolution). The following statements relate the CPU clock, with the A/D Converter, and the total amount of time required to fill the data buffer.

- ADC_clk       = CPU_clk/Prescaling = 20 MHz / 128 = 156.25 kHz
- Sample_clk    = ADC_clk/Conversion_Cycles = 156.25 kHz / 13.5 = 11.574 kHz
- Buffer_Size   = 128
- Buffer_Period = (1/Sample_clk)*Buffer_Size ~= 11.0592 ms

With the buffer filled, the data is transferred to the FFT input so we can start to collect the next samples, meanwhile the FFT is running. Giving this condition, the FFT runtime plus others functions (like ISR's from the Timers, or even the ADC sampling routine) cannot be higher than 11 ms.


<!-- ----------- -->
## TODO List

- apply a window (hanning, hamming, etc.) on the input signal before the FFT
- design an analogic filter to avoid frequencies above the nyquist
