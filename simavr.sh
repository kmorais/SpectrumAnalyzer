#!/usr/bin/bash

# ---------------------------------------------------
# This script start the SIMAVR simulator, so we can
# connect avr-gdb to it. By default, the connection
# is localhost:1234.
# ---------------------------------------------------

echo -e "\n->Starting SIMAVR...\n"

# ---
# Setup
# NOTE: directory not relative
DIR=/home/kmorais/Development/AVR/Spectrum-Analyzer/
ELF=build/main

DEVICE=atmega328p
FREQ=20000000    # MHz

# ---
# Run
cd "$DIR"
simavr -g -f $FREQ -m $DEVICE $ELF

echo -e "\n->Exiting SIMAVR...\n"
