EESchema Schematic File Version 4
LIBS:pcb-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C_clk1
U 1 1 5DD61B65
P 4000 3500
F 0 "C_clk1" V 3850 3600 50  0000 C CNN
F 1 "22pf" V 3950 3550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4038 3350 50  0001 C CNN
F 3 "~" H 4000 3500 50  0001 C CNN
	1    4000 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C_clk0
U 1 1 5DD638D6
P 4000 3200
F 0 "C_clk0" V 4150 3200 50  0000 L CNN
F 1 "22pf" V 4050 3250 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4038 3050 50  0001 C CNN
F 3 "~" H 4000 3200 50  0001 C CNN
	1    4000 3200
	0    -1   -1   0   
$EndComp
Text GLabel 4800 2400 2    50   Output ~ 0
GND
Text GLabel 1600 3750 2    50   Input ~ 0
GND
$Comp
L Device:Crystal Crystal0
U 1 1 5DD80F7E
P 3650 3350
F 0 "Crystal0" V 3650 3250 50  0001 R CNN
F 1 "20MHz" V 3650 3000 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 3650 3350 50  0001 C CNN
F 3 "~" H 3650 3350 50  0001 C CNN
	1    3650 3350
	0    1    1    0   
$EndComp
Connection ~ 3650 3200
Wire Wire Line
	3650 3200 3850 3200
Wire Wire Line
	4150 3200 4150 3350
$Comp
L 74xx:74HC595 U5
U 1 1 5DD6FB20
P 3050 6700
F 0 "U5" H 3050 6700 50  0000 C CNN
F 1 "SN74HC595__R8:15" H 3050 7650 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 3050 6700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 3050 6700 50  0001 C CNN
	1    3050 6700
	1    0    0    -1  
$EndComp
Text GLabel 1600 7500 3    50   Input ~ 0
GND
Text GLabel 3050 7500 3    50   Input ~ 0
GND
Text GLabel 1600 6000 1    50   Input ~ 0
Vcc
$Comp
L 74xx:74HC595 U4
U 1 1 5DD6DE37
P 1600 6700
F 0 "U4" H 1600 6700 50  0000 C CNN
F 1 "SN74HC595__R0:7" H 1600 7650 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 1600 6700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 1600 6700 50  0001 C CNN
	1    1600 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 7400 3050 7450
Wire Wire Line
	1600 6100 1600 6050
Wire Wire Line
	1600 7400 1600 7450
Text GLabel 3050 6000 1    50   Input ~ 0
Vcc
Wire Wire Line
	3050 6000 3050 6050
Text GLabel 2300 2750 2    50   Input ~ 0
PD0
Text GLabel 2300 2850 2    50   Input ~ 0
PD1
Text GLabel 2300 2950 2    50   Input ~ 0
PD2
Text GLabel 2300 3050 2    50   Input ~ 0
PD3
Text GLabel 2300 3150 2    50   Input ~ 0
PD4
Text GLabel 2600 3250 2    50   Input ~ 0
PD5
Text GLabel 2600 3350 2    50   Input ~ 0
PD6
Text GLabel 2300 3450 2    50   Input ~ 0
PD7
Wire Wire Line
	2100 2750 2300 2750
Wire Wire Line
	2100 2850 2300 2850
Wire Wire Line
	2100 2950 2300 2950
Wire Wire Line
	2100 3050 2300 3050
Wire Wire Line
	2100 3150 2300 3150
Wire Wire Line
	2100 3450 2300 3450
Text GLabel 2600 6500 0    50   Input ~ 0
SCLK
Text GLabel 2600 6800 0    50   Input ~ 0
RCLK
Text GLabel 2600 6300 0    50   Input ~ 0
PD7
Text GLabel 1150 6500 0    50   Input ~ 0
SCLK
Text GLabel 1150 6800 0    50   Input ~ 0
RCLK
Wire Wire Line
	2600 6300 2650 6300
Text GLabel 1150 6300 0    50   Input ~ 0
PD4
Wire Wire Line
	1200 6300 1150 6300
Wire Wire Line
	1200 6900 1150 6900
Wire Wire Line
	1150 6900 1150 7450
Wire Wire Line
	1150 7450 1600 7450
Connection ~ 1600 7450
Wire Wire Line
	1600 7450 1600 7500
Wire Wire Line
	2650 6900 2600 6900
Wire Wire Line
	2600 6900 2600 7450
Wire Wire Line
	2600 7450 3050 7450
Connection ~ 3050 7450
Wire Wire Line
	3050 7450 3050 7500
Wire Wire Line
	2650 6600 2300 6600
Wire Wire Line
	2300 6600 2300 6050
Wire Wire Line
	2300 6050 3050 6050
Connection ~ 3050 6050
Wire Wire Line
	3050 6050 3050 6100
Wire Wire Line
	1200 6600 850  6600
Wire Wire Line
	850  6600 850  6050
Wire Wire Line
	850  6050 1600 6050
Connection ~ 1600 6050
Wire Wire Line
	1600 6050 1600 6000
Wire Wire Line
	1150 6500 1200 6500
Wire Wire Line
	1150 6800 1200 6800
Wire Wire Line
	2600 6800 2650 6800
Wire Wire Line
	2600 6500 2650 6500
$Comp
L Device:C C_U5
U 1 1 5DE1FE26
P 2300 7300
F 0 "C_U5" H 2300 7500 50  0000 L CNN
F 1 "0.1uf" H 2300 7400 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2338 7150 50  0001 C CNN
F 3 "~" H 2300 7300 50  0001 C CNN
	1    2300 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C_U4
U 1 1 5DE23678
P 850 7300
F 0 "C_U4" H 850 7500 50  0000 L CNN
F 1 "0.1uf" H 850 7400 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 888 7150 50  0001 C CNN
F 3 "~" H 850 7300 50  0001 C CNN
	1    850  7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  7450 1150 7450
Connection ~ 1150 7450
Wire Wire Line
	850  7150 850  6600
Connection ~ 850  6600
Wire Wire Line
	2300 7450 2600 7450
Connection ~ 2600 7450
Wire Wire Line
	2300 7150 2300 6600
Connection ~ 2300 6600
Text GLabel 2300 1950 2    50   Input ~ 0
PC0
Text GLabel 2300 2050 2    50   Input ~ 0
PC1
Text GLabel 2300 2150 2    50   Input ~ 0
PC2
Text GLabel 2300 2250 2    50   Input ~ 0
PC3
Text GLabel 2300 2350 2    50   Input ~ 0
PC4
Text GLabel 2300 2450 2    50   Input ~ 0
PC5
Text GLabel 2300 2550 2    50   Input ~ 0
PC6
Wire Wire Line
	2100 2050 2300 2050
Wire Wire Line
	2100 2150 2300 2150
Wire Wire Line
	2100 2250 2300 2250
Wire Wire Line
	2100 2350 2300 2350
Wire Wire Line
	2100 2450 2300 2450
Wire Wire Line
	2100 2550 2300 2550
Text GLabel 2300 1050 2    50   Input ~ 0
PB0
Text GLabel 2300 1150 2    50   Input ~ 0
PB1
Text GLabel 2300 1250 2    50   Input ~ 0
PB2
Text GLabel 2300 1350 2    50   Input ~ 0
PB3
Text GLabel 2300 1450 2    50   Input ~ 0
PB4
Text GLabel 2300 1550 2    50   Input ~ 0
PB5
Wire Wire Line
	2100 1050 2300 1050
Wire Wire Line
	2100 1150 2300 1150
Wire Wire Line
	2100 1250 2300 1250
Wire Wire Line
	2100 1350 2300 1350
Wire Wire Line
	2100 1450 2300 1450
Wire Wire Line
	2100 1550 2300 1550
Text GLabel 7650 4850 0    50   Input ~ 0
PB0
Text GLabel 7650 4950 0    50   Input ~ 0
PB1
Text GLabel 7650 5050 0    50   Input ~ 0
PB2
Text GLabel 7650 5150 0    50   Input ~ 0
PB3
Wire Wire Line
	7850 4850 7650 4850
Wire Wire Line
	7850 4950 7650 4950
Wire Wire Line
	7850 5050 7650 5050
Wire Wire Line
	7850 5150 7650 5150
Text GLabel 7650 5550 0    50   Input ~ 0
GND
Wire Wire Line
	7850 5550 7650 5550
Text GLabel 7650 4450 0    50   Input ~ 0
Vcc
Wire Wire Line
	7850 4450 7650 4450
$Comp
L Transistor_Array:ULN2803A U2
U 1 1 5DEA0D01
P 9850 4250
F 0 "U2" H 9850 4250 50  0000 C CNN
F 1 "ULN2803A__C0:7" H 9250 4650 50  0000 C CNN
F 2 "Package_DIP:DIP-18_W7.62mm" H 9900 3600 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2803a.pdf" H 9950 4050 50  0001 C CNN
	1    9850 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4250 9050 4050
Wire Wire Line
	9050 4050 9450 4050
Wire Wire Line
	9100 4350 9100 4150
Wire Wire Line
	9100 4150 9450 4150
Wire Wire Line
	9150 4450 9150 4250
Wire Wire Line
	9150 4250 9450 4250
Wire Wire Line
	9200 4550 9200 4350
Wire Wire Line
	9200 4350 9450 4350
Wire Wire Line
	9250 4650 9250 4450
Wire Wire Line
	9250 4450 9450 4450
Wire Wire Line
	9300 4750 9300 4550
Wire Wire Line
	9300 4550 9450 4550
Wire Wire Line
	9350 4850 9350 4650
Wire Wire Line
	9350 4650 9450 4650
Wire Wire Line
	9400 4950 9400 4750
Wire Wire Line
	9400 4750 9450 4750
Wire Wire Line
	9400 5050 9400 5450
Wire Wire Line
	9400 5450 9450 5450
Wire Wire Line
	9350 5150 9350 5550
Wire Wire Line
	9350 5550 9450 5550
Wire Wire Line
	9450 5650 9300 5650
Wire Wire Line
	9300 5650 9300 5250
Wire Wire Line
	9250 5350 9250 5750
Wire Wire Line
	9250 5750 9450 5750
Wire Wire Line
	9450 5850 9200 5850
Wire Wire Line
	9200 5850 9200 5450
Wire Wire Line
	9150 5550 9150 5950
Wire Wire Line
	9150 5950 9450 5950
Wire Wire Line
	9450 6050 9100 6050
Wire Wire Line
	9100 6050 9100 5650
Wire Wire Line
	9100 5650 9050 5650
Wire Wire Line
	9050 5750 9050 6150
Wire Wire Line
	9050 6150 9450 6150
Text GLabel 9900 6350 2    50   Input ~ 0
GND
Text GLabel 9900 4950 2    50   Input ~ 0
GND
Text GLabel 8400 5900 0    50   Input ~ 0
GND
Wire Wire Line
	8450 5900 8400 5900
Text GLabel 8500 4000 2    50   Input ~ 0
Vcc
Wire Wire Line
	8500 4000 8450 4000
$Comp
L MCU_Microchip_ATmega:ATmega328P-PU U0
U 1 1 5DD5ED11
P 1500 2250
F 0 "U0" H 1500 2250 50  0000 R CNN
F 1 "ATMega328p" H 1700 2150 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 856 2159 50  0001 R CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 1500 2250 50  0001 C CNN
	1    1500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3750 1600 3750
Text GLabel 800  1050 0    50   Input ~ 0
AREF
Text GLabel 2300 1650 2    50   Input ~ 0
PB6
Text GLabel 2300 1750 2    50   Input ~ 0
PB7
Wire Wire Line
	2100 1650 2300 1650
Wire Wire Line
	2100 1750 2300 1750
Text GLabel 3300 3500 0    50   Input ~ 0
PB7
Text GLabel 3300 3200 0    50   Input ~ 0
PB6
Wire Wire Line
	3300 3200 3650 3200
Connection ~ 4150 3350
Wire Wire Line
	4150 3350 4150 3500
Text Notes 7350 7500 0    50   ~ 0
Spectrum Analyzer
Text GLabel 8200 2850 2    50   Input ~ 0
Audio_+
Text GLabel 8200 2950 2    50   Input ~ 0
Audio_-
Text Notes 700  5650 0    129  ~ 0
ATMega328p <==> Shift Register (Row 0:15)\n
Wire Wire Line
	9900 6350 9850 6350
Wire Wire Line
	9900 4950 9850 4950
Text Notes 7100 3700 0    90   ~ 0
ATMega328p PB3:0 <=> 4_16 DEC <=> ULN2803 C0:15
Wire Wire Line
	9050 4350 9100 4350
Wire Wire Line
	9050 4450 9150 4450
Wire Wire Line
	9050 4550 9200 4550
Wire Wire Line
	9050 4650 9250 4650
Wire Wire Line
	9050 4750 9300 4750
Wire Wire Line
	9050 5550 9150 5550
Wire Wire Line
	9050 4850 9350 4850
Wire Wire Line
	9050 4950 9400 4950
Wire Wire Line
	9300 5250 9050 5250
Wire Wire Line
	9050 5350 9250 5350
Wire Wire Line
	9050 5050 9400 5050
Wire Wire Line
	9050 5150 9350 5150
Wire Wire Line
	9200 5450 9050 5450
$Comp
L 4xxx_IEEE:4514 U1
U 1 1 5DE6E968
P 8450 5000
F 0 "U1" H 8450 5000 50  0000 C CNN
F 1 "CD4514__C0:15" H 8050 5850 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 8450 5000 50  0001 C CNN
F 3 "" H 8450 5000 50  0001 C CNN
	1    8450 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4000 8450 4200
Wire Wire Line
	8450 5900 8450 5800
Text GLabel 3300 2250 0    50   Input ~ 0
PC3
Wire Wire Line
	3300 2250 3650 2250
Text Notes 3300 2050 0    50   ~ 0
Effect 2
Text Notes 3300 2250 0    50   ~ 0
Effect 1
Text GLabel 3300 1850 0    50   Input ~ 0
PC6
Text Notes 3300 1850 0    50   ~ 0
Reset PIN
Text GLabel 3300 2050 0    50   Input ~ 0
PC4
Text Notes 3300 2450 0    50   ~ 0
Delay PIN
$Comp
L Amplifier_Operational:LM741 U_f0
U 1 1 5DD9BB91
P 9400 1650
F 0 "U_f0" H 9200 1650 50  0000 L CNN
F 1 "LM741" H 8950 1650 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 9450 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 9550 1800 50  0001 C CNN
	1    9400 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_f1
U 1 1 5DD9D036
P 8750 1550
F 0 "R_f1" V 8543 1550 50  0000 C CNN
F 1 "10k" V 8634 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8680 1550 50  0001 C CNN
F 3 "~" H 8750 1550 50  0001 C CNN
	1    8750 1550
	0    1    1    0   
$EndComp
$Comp
L Device:R R_f0
U 1 1 5DD9D76F
P 8350 1550
F 0 "R_f0" V 8143 1550 50  0000 C CNN
F 1 "5k" V 8234 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8280 1550 50  0001 C CNN
F 3 "~" H 8350 1550 50  0001 C CNN
	1    8350 1550
	0    1    1    0   
$EndComp
$Comp
L Device:R R_f3
U 1 1 5DD9DD85
P 8750 2150
F 0 "R_f3" V 8543 2150 50  0000 C CNN
F 1 "1k" V 8634 2150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8680 2150 50  0001 C CNN
F 3 "~" H 8750 2150 50  0001 C CNN
	1    8750 2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R_f2
U 1 1 5DD9EB95
P 8350 2150
F 0 "R_f2" V 8143 2150 50  0000 C CNN
F 1 "10k" V 8234 2150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8280 2150 50  0001 C CNN
F 3 "~" H 8350 2150 50  0001 C CNN
	1    8350 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	8500 2150 8550 2150
Connection ~ 8550 2150
Wire Wire Line
	8550 2150 8600 2150
Wire Wire Line
	8500 1550 8550 1550
Wire Wire Line
	8200 1550 7950 1550
Text GLabel 9350 1350 2    50   Input ~ 0
+Vcc_f
Wire Wire Line
	9350 1350 9300 1350
$Comp
L Device:C C_f1
U 1 1 5DDDCDE9
P 9350 1050
F 0 "C_f1" V 9098 1050 50  0000 C CNN
F 1 "2.2nf" V 9189 1050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 9388 900 50  0001 C CNN
F 3 "~" H 9350 1050 50  0001 C CNN
	1    9350 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	9500 1050 9700 1050
Wire Wire Line
	9700 1050 9700 1650
Wire Wire Line
	9200 1050 8550 1050
Wire Wire Line
	8550 1050 8550 1550
Connection ~ 8550 1550
Wire Wire Line
	8550 1550 8600 1550
Text GLabel 8100 2150 0    50   Input ~ 0
GND_f
Wire Wire Line
	8100 2150 8200 2150
Text GLabel 8900 1150 0    50   Input ~ 0
GND_f
$Comp
L Device:C C_f0
U 1 1 5DE4B65D
P 9000 1400
F 0 "C_f0" H 9050 1600 50  0000 L CNN
F 1 "2.2nf" H 9050 1500 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 9038 1250 50  0001 C CNN
F 3 "~" H 9000 1400 50  0001 C CNN
	1    9000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2050 9300 1950
Text GLabel 9350 2050 2    50   Input ~ 0
-Vcc_f
Wire Wire Line
	9350 2050 9300 2050
Wire Wire Line
	9700 1650 9700 2150
Wire Wire Line
	9700 2150 8900 2150
Connection ~ 9700 1650
Wire Wire Line
	8550 1750 9100 1750
Wire Wire Line
	8550 1750 8550 2150
Wire Wire Line
	9000 1150 8900 1150
Wire Wire Line
	9000 1150 9000 1250
Wire Wire Line
	8900 1550 9000 1550
Wire Wire Line
	9100 1550 9000 1550
Connection ~ 9000 1550
$Comp
L Diode:1N47xxA D_Zenner0
U 1 1 5DECEA28
P 3900 1300
F 0 "D_Zenner0" H 3700 1500 50  0000 L CNN
F 1 "1N47xxA" H 3750 1400 50  0000 L CNN
F 2 "Diode_THT:D_DO-34_SOD68_P7.62mm_Horizontal" H 3900 1125 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 3900 1300 50  0001 C CNN
	1    3900 1300
	1    0    0    -1  
$EndComp
Text GLabel 3300 1300 0    50   Input ~ 0
PC5
Text Notes 7350 750  0    90   ~ 0
2ª Order Low Pass Filter - Variable Gain
Text GLabel 9800 2150 2    50   Input ~ 0
Filter_out
Text GLabel 7950 1550 0    50   Input ~ 0
Audio_+
$Comp
L Regulator_SwitchedCapacitor:ICL7660 U_f1
U 1 1 5E17CFCF
P 9900 2800
F 0 "U_f1" H 9900 2700 50  0000 C CNN
F 1 "ICL7760" H 10200 2350 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10000 2700 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/ICL7660-MAX1044.pdf" H 10000 2700 50  0001 C CNN
	1    9900 2800
	1    0    0    -1  
$EndComp
Text GLabel 9450 3350 0    50   Input ~ 0
GND_f
Wire Wire Line
	9450 3350 9500 3350
Wire Wire Line
	9900 3350 9900 3300
Text GLabel 9400 2500 0    50   Input ~ 0
+Vcc_f
Wire Wire Line
	9400 2500 9450 2500
Text GLabel 10700 2400 2    50   Input ~ 0
-Vcc_f
Wire Wire Line
	9500 3000 9500 3350
Connection ~ 9500 3350
Wire Wire Line
	9500 3350 9900 3350
$Comp
L Device:CP1_Small C_f3
U 1 1 5E22E92B
P 10300 2900
F 0 "C_f3" H 10391 2946 50  0000 L CNN
F 1 "10uf" H 10391 2855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 10300 2900 50  0001 C CNN
F 3 "~" H 10300 2900 50  0001 C CNN
	1    10300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 2800 10300 2700
Wire Wire Line
	10300 3000 10300 3100
$Comp
L Device:CP1_Small C_f5
U 1 1 5E268F58
P 10400 2550
F 0 "C_f5" H 10300 2500 50  0000 R CNN
F 1 "10uf" H 10300 2600 50  0000 R CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 10400 2550 50  0001 C CNN
F 3 "~" H 10400 2550 50  0001 C CNN
	1    10400 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	10300 2400 10400 2400
Wire Wire Line
	10400 2400 10700 2400
Connection ~ 10400 2400
Wire Wire Line
	10400 2700 10400 2650
Wire Wire Line
	10400 2700 10700 2700
Text GLabel 10700 2700 2    50   Input ~ 0
GND_f
Wire Wire Line
	10400 2450 10400 2400
Wire Wire Line
	10300 2400 10300 2500
$Comp
L Device:CP1_Small C_f4
U 1 1 5E3182FE
P 9300 2800
F 0 "C_f4" H 9250 2800 50  0000 R CNN
F 1 "10uf" H 9250 2700 50  0000 R CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 9300 2800 50  0001 C CNN
F 3 "~" H 9300 2800 50  0001 C CNN
	1    9300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2900 9300 3000
Wire Wire Line
	9300 3000 9500 3000
Connection ~ 9500 3000
Wire Wire Line
	9300 2700 9300 2650
Wire Wire Line
	9300 2650 9450 2650
Wire Wire Line
	9450 2650 9450 2500
Connection ~ 9450 2500
Wire Wire Line
	9450 2500 9500 2500
Text Notes 9500 2350 0    50   ~ 0
-Vcc Power Supply
Text GLabel 2050 6300 2    50   Input ~ 0
R0
Text GLabel 2050 6400 2    50   Input ~ 0
R1
Text GLabel 2050 6500 2    50   Input ~ 0
R2
Text GLabel 2050 6600 2    50   Input ~ 0
R3
Text GLabel 2050 6700 2    50   Input ~ 0
R4
Text GLabel 2050 6800 2    50   Input ~ 0
R5
Text GLabel 2050 6900 2    50   Input ~ 0
R6
Text GLabel 2050 7000 2    50   Input ~ 0
R7
Wire Wire Line
	2000 6300 2050 6300
Wire Wire Line
	2050 6400 2000 6400
Wire Wire Line
	2000 6500 2050 6500
Wire Wire Line
	2000 6600 2050 6600
Wire Wire Line
	2000 6700 2050 6700
Wire Wire Line
	2000 6800 2050 6800
Wire Wire Line
	2000 6900 2050 6900
Wire Wire Line
	2000 7000 2050 7000
Text GLabel 3500 6300 2    50   Input ~ 0
R8
Text GLabel 3500 6400 2    50   Input ~ 0
R9
Text GLabel 3500 6500 2    50   Input ~ 0
R10
Text GLabel 3500 6600 2    50   Input ~ 0
R11
Text GLabel 3500 6700 2    50   Input ~ 0
R12
Text GLabel 3500 6800 2    50   Input ~ 0
R13
Text GLabel 3500 6900 2    50   Input ~ 0
R14
Text GLabel 3500 7000 2    50   Input ~ 0
R15
Wire Wire Line
	3450 6300 3500 6300
Wire Wire Line
	3500 6400 3450 6400
Wire Wire Line
	3450 6500 3500 6500
Wire Wire Line
	3450 6600 3500 6600
Wire Wire Line
	3450 6700 3500 6700
Wire Wire Line
	3450 6800 3500 6800
Wire Wire Line
	3450 6900 3500 6900
Wire Wire Line
	3450 7000 3500 7000
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	6900 7750 650  7750
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	650  3850 6900 3850
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	6900 3850 6900 7750
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	650  3850 650  7750
$Comp
L Device:R R_Q2
U 1 1 5E7B964F
P 12850 1650
F 0 "R_Q2" V 12950 1650 50  0000 C CNN
F 1 "R" V 12734 1650 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 1650 50  0001 C CNN
F 3 "~" H 12850 1650 50  0001 C CNN
	1    12850 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q1
U 1 1 5E7B9645
P 12850 1100
F 0 "R_Q1" V 12950 1100 50  0000 C CNN
F 1 "R" V 12734 1100 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 1100 50  0001 C CNN
F 3 "~" H 12850 1100 50  0001 C CNN
	1    12850 1100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q0
U 1 1 5E7B963B
P 12850 550
F 0 "R_Q0" V 12950 550 50  0000 C CNN
F 1 "R" V 12734 550 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 550 50  0001 C CNN
F 3 "~" H 12850 550 50  0001 C CNN
	1    12850 550 
	0    1    -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q0
U 1 1 5E7B962B
P 13400 550
F 0 "Q0" H 13450 550 50  0000 L CNN
F 1 "BC328" V 13600 450 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 475 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 550 50  0001 L CNN
	1    13400 550 
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q1
U 1 1 5E7B9621
P 13400 1100
F 0 "Q1" H 13450 1100 50  0000 L CNN
F 1 "BC328" V 13600 1000 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 1025 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 1100 50  0001 L CNN
	1    13400 1100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q2
U 1 1 5E7B9617
P 13400 1650
F 0 "Q2" H 13450 1650 50  0000 L CNN
F 1 "BC328" V 13600 1550 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 1575 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 1650 50  0001 L CNN
	1    13400 1650
	1    0    0    -1  
$EndComp
Text Notes 13300 200  0    129  ~ 0
Current Source - R_Qn = 2.5k\n
Wire Wire Line
	9800 2150 9700 2150
Connection ~ 9700 2150
Wire Wire Line
	9800 1650 9700 1650
$Comp
L Device:R R_f5
U 1 1 5DFEB33E
P 10250 1850
F 0 "R_f5" H 10400 1900 50  0000 C CNN
F 1 "10k" H 10400 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10180 1850 50  0001 C CNN
F 3 "~" H 10250 1850 50  0001 C CNN
	1    10250 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1650 10250 1650
$Comp
L Device:C C_f2
U 1 1 5E0FA134
P 9950 1650
F 0 "C_f2" V 10202 1650 50  0000 C CNN
F 1 "100mf" V 10111 1650 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 9988 1500 50  0001 C CNN
F 3 "~" H 9950 1650 50  0001 C CNN
	1    9950 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10250 1650 10250 1700
Connection ~ 10250 1650
Wire Wire Line
	10250 1650 10500 1650
Wire Wire Line
	10250 1600 10250 1650
Wire Wire Line
	10300 2050 10250 2050
Text GLabel 10300 2050 2    50   Input ~ 0
GND_f
Wire Wire Line
	10300 1300 10250 1300
Text GLabel 10300 1300 2    50   Input ~ 0
+Vcc_f
$Comp
L Device:R R_f4
U 1 1 5DFEA63A
P 10250 1450
F 0 "R_f4" H 10100 1450 50  0000 C CNN
F 1 "10k" H 10100 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10180 1450 50  0001 C CNN
F 3 "~" H 10250 1450 50  0001 C CNN
	1    10250 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	10250 2050 10250 2000
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	7000 550  11150 550 
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	7000 3450 11150 3450
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	11150 550  11150 3450
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	7000 550  7000 3450
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	7000 6450 11150 6450
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	7000 3550 11150 3550
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	11150 6450 11150 3550
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	7000 6450 7000 3550
$Comp
L Device:C C_U1
U 1 1 5E7B8508
P 8200 4000
F 0 "C_U1" V 8400 3800 50  0000 L CNN
F 1 "0.1uf" V 8300 3750 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 8238 3850 50  0001 C CNN
F 3 "~" H 8200 4000 50  0001 C CNN
	1    8200 4000
	0    -1   -1   0   
$EndComp
Text GLabel 7950 4000 0    50   Input ~ 0
GND
Wire Wire Line
	8350 4000 8450 4000
Connection ~ 8450 4000
Wire Wire Line
	7950 4000 8050 4000
$Comp
L Device:C C_U0_0
U 1 1 5E8A74A2
P 1650 600
F 0 "C_U0_0" V 1700 250 50  0000 L CNN
F 1 "0.1uf" V 1700 650 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 1688 450 50  0001 C CNN
F 3 "~" H 1650 600 50  0001 C CNN
	1    1650 600 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 750  1600 750 
Connection ~ 1500 750 
Wire Wire Line
	1500 750  1500 600 
$Comp
L Device:C C_U0_1
U 1 1 5EA0E4D2
P 1950 750
F 0 "C_U0_1" V 2000 400 50  0000 L CNN
F 1 "0.1uf" V 2000 800 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 1988 600 50  0001 C CNN
F 3 "~" H 1950 750 50  0001 C CNN
	1    1950 750 
	0    -1   -1   0   
$EndComp
Text GLabel 2300 600  2    50   Input ~ 0
GND
Wire Wire Line
	2300 600  1800 600 
Wire Wire Line
	1800 750  1600 750 
Connection ~ 1600 750 
Wire Wire Line
	2100 750  2300 750 
Wire Wire Line
	2300 750  2300 600 
$Comp
L Connector_Generic:Conn_01x08 CONN_C8:15
U 1 1 5DDDBA1D
P 10600 5750
F 0 "CONN_C8:15" H 10680 5742 50  0000 L CNN
F 1 "Conn_01x08" H 10680 5651 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 10600 5750 50  0001 C CNN
F 3 "~" H 10600 5750 50  0001 C CNN
	1    10600 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 CONN_C0:7
U 1 1 5DDDFA37
P 10600 4350
F 0 "CONN_C0:7" H 10680 4342 50  0000 L CNN
F 1 "Conn_01x08" H 10680 4251 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 10600 4350 50  0001 C CNN
F 3 "~" H 10600 4350 50  0001 C CNN
	1    10600 4350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 CONN_R_in_0:7
U 1 1 5DDFFA3B
P 12100 950
F 0 "CONN_R_in_0:7" H 11650 450 50  0000 L CNN
F 1 "Conn_01x08" H 11700 350 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 12100 950 50  0001 C CNN
F 3 "~" H 12100 950 50  0001 C CNN
	1    12100 950 
	-1   0    0    1   
$EndComp
$Comp
L Transistor_Array:ULN2803A U3
U 1 1 5DEB79DB
P 9850 5650
F 0 "U3" H 9850 5650 50  0000 C CNN
F 1 "ULN2803A__C8:15" H 9200 5050 50  0000 C CNN
F 2 "Package_DIP:DIP-18_W7.62mm" H 9900 5000 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2803a.pdf" H 9950 5450 50  0001 C CNN
	1    9850 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 5450 10400 5450
Wire Wire Line
	10250 5550 10400 5550
Wire Wire Line
	10250 5650 10400 5650
Wire Wire Line
	10250 5750 10400 5750
Wire Wire Line
	10250 5850 10400 5850
Wire Wire Line
	10250 5950 10400 5950
Wire Wire Line
	10250 6050 10400 6050
Wire Wire Line
	10250 6150 10400 6150
Wire Wire Line
	10250 4750 10400 4750
Wire Wire Line
	10250 4650 10400 4650
Wire Wire Line
	10250 4550 10400 4550
Wire Wire Line
	10250 4450 10400 4450
Wire Wire Line
	10250 4350 10400 4350
Wire Wire Line
	10250 4250 10400 4250
Wire Wire Line
	10250 4150 10400 4150
Wire Wire Line
	10250 4050 10400 4050
$Comp
L Device:R R_rst0
U 1 1 5E1658AF
P 3600 1650
F 0 "R_rst0" V 3500 1650 50  0000 C CNN
F 1 "10k" V 3600 1650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 1650 50  0001 C CNN
F 3 "~" H 3600 1650 50  0001 C CNN
	1    3600 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 2400 4800 2400
Text GLabel 3300 1650 0    50   Output ~ 0
Vcc
Connection ~ 4750 2400
Wire Wire Line
	3300 1300 3400 1300
Text Notes 3100 3150 0    50   ~ 0
Clock PINs\n
Text Notes 3100 1250 0    50   ~ 0
ADC Input
Wire Wire Line
	4750 2400 4750 3350
Wire Wire Line
	800  1050 900  1050
Text GLabel 800  750  0    50   Input ~ 0
Vcc
Wire Wire Line
	800  750  1500 750 
Wire Wire Line
	3300 1850 3750 1850
Wire Wire Line
	3750 1650 3750 1850
Wire Wire Line
	3300 1650 3450 1650
Wire Wire Line
	3650 2250 3650 2200
Wire Wire Line
	3750 2300 3750 2450
Wire Wire Line
	3300 2450 3750 2450
Wire Wire Line
	3300 2050 3650 2050
Wire Wire Line
	3650 2050 3650 2100
Wire Wire Line
	3750 2000 3750 1850
Connection ~ 3750 1850
Text GLabel 3300 1400 0    50   Input ~ 0
ADC
Wire Wire Line
	2100 1950 2300 1950
Wire Wire Line
	3300 1400 3400 1400
Wire Wire Line
	3400 1400 3400 1300
Connection ~ 3400 1300
Wire Wire Line
	3400 1300 3750 1300
Text GLabel 10500 1650 2    50   Input ~ 0
ADC
$Comp
L Connector_Generic:Conn_01x02 CONN_POWER0
U 1 1 5ED00BC5
P 3650 800
F 0 "CONN_POWER0" H 3730 792 50  0000 L CNN
F 1 "Conn_01x02" H 3730 701 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 3650 800 50  0001 C CNN
F 3 "~" H 3650 800 50  0001 C CNN
	1    3650 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 950  3300 950 
Text GLabel 3300 750  0    50   Input ~ 0
Vcc
Text GLabel 3300 950  0    50   Input ~ 0
GND
Wire Wire Line
	3300 750  3350 750 
Wire Wire Line
	3350 750  3350 800 
Wire Wire Line
	3350 800  3450 800 
Wire Wire Line
	3350 950  3350 900 
Wire Wire Line
	3350 900  3450 900 
Text Notes 3100 700  0    50   ~ 0
Power Input
$Comp
L Connector_Generic:Conn_01x02 CON_AUDIO1
U 1 1 5EECC8B2
P 7750 2950
F 0 "CON_AUDIO1" H 8050 2850 50  0000 C CNN
F 1 "Conn_01x02" H 8050 2950 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 7750 2950 50  0001 C CNN
F 3 "~" H 7750 2950 50  0001 C CNN
	1    7750 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	7950 2850 8200 2850
Wire Wire Line
	8200 2950 7950 2950
Wire Wire Line
	3300 3500 3650 3500
Connection ~ 3650 3500
Wire Wire Line
	3850 3500 3650 3500
Text GLabel 3300 2450 0    50   Input ~ 0
PC2
Wire Wire Line
	4000 6200 4250 6200
Wire Wire Line
	4000 6400 4250 6400
Wire Wire Line
	4000 6500 4250 6500
Wire Wire Line
	4000 6600 4250 6600
Wire Wire Line
	4000 6300 4250 6300
Wire Wire Line
	4000 6800 4250 6800
Wire Wire Line
	4000 6900 4250 6900
Wire Wire Line
	4000 7000 4250 7000
Wire Wire Line
	4000 6700 4250 6700
Wire Wire Line
	4000 7200 4250 7200
Wire Wire Line
	4000 7300 4250 7300
Wire Wire Line
	4000 7400 4250 7400
Wire Wire Line
	4000 7100 4250 7100
$Comp
L Connector_Generic:Conn_01x08 CONN_R8:15
U 1 1 5DE34EB3
P 4450 7000
F 0 "CONN_R8:15" H 4530 6992 50  0000 L CNN
F 1 "Conn_01x08" H 4530 6901 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 4450 7000 50  0001 C CNN
F 3 "~" H 4450 7000 50  0001 C CNN
	1    4450 7000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 CONN_R0:7
U 1 1 5DE34EB9
P 4450 6300
F 0 "CONN_R0:7" H 4530 6292 50  0000 L CNN
F 1 "Conn_01x08" H 4530 6201 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 4450 6300 50  0001 C CNN
F 3 "~" H 4450 6300 50  0001 C CNN
	1    4450 6300
	1    0    0    1   
$EndComp
Wire Wire Line
	4000 5900 4250 5900
Wire Wire Line
	4000 6100 4250 6100
Wire Wire Line
	4000 6000 4250 6000
Text GLabel 4000 6700 0    50   Input ~ 0
R8
Text GLabel 4000 6800 0    50   Input ~ 0
R9
Text GLabel 4000 6900 0    50   Input ~ 0
R10
Text GLabel 4000 7000 0    50   Input ~ 0
R11
Text GLabel 4000 7100 0    50   Input ~ 0
R12
Text GLabel 4000 7200 0    50   Input ~ 0
R13
Text GLabel 4000 7300 0    50   Input ~ 0
R14
Text GLabel 4000 7400 0    50   Input ~ 0
R15
Text GLabel 4000 5900 0    50   Input ~ 0
R0
Text GLabel 4000 6000 0    50   Input ~ 0
R1
Text GLabel 4000 6100 0    50   Input ~ 0
R2
Text GLabel 4000 6200 0    50   Input ~ 0
R3
Text GLabel 4000 6300 0    50   Input ~ 0
R4
Text GLabel 4000 6400 0    50   Input ~ 0
R5
Text GLabel 4000 6500 0    50   Input ~ 0
R6
Text GLabel 4000 6600 0    50   Input ~ 0
R7
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	12000 0    18200 0   
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	12000 0    12000 4800
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	18200 4800 12000 4800
Wire Wire Line
	12700 650  12700 1100
Wire Wire Line
	13000 550  13200 550 
Wire Wire Line
	13500 750  13150 750 
Connection ~ 13150 1300
Wire Wire Line
	13150 1300 13150 1850
Wire Wire Line
	13000 1650 13200 1650
Wire Wire Line
	12650 1650 12700 1650
Wire Wire Line
	13150 750  13150 1300
Wire Wire Line
	13000 1100 13200 1100
Wire Wire Line
	13150 1300 13500 1300
Wire Wire Line
	13500 1850 13150 1850
Connection ~ 13150 1850
Wire Wire Line
	13150 1850 13150 2400
$Comp
L Device:R R_Q3
U 1 1 5F69570D
P 12850 2200
F 0 "R_Q3" V 12950 2200 50  0000 C CNN
F 1 "R" V 12734 2200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 2200 50  0001 C CNN
F 3 "~" H 12850 2200 50  0001 C CNN
	1    12850 2200
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q3
U 1 1 5F695713
P 13400 2200
F 0 "Q3" H 13450 2200 50  0000 L CNN
F 1 "BC328" V 13600 2100 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 2125 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 2200 50  0001 L CNN
	1    13400 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 2200 13200 2200
Wire Wire Line
	13500 2400 13150 2400
Connection ~ 13150 2400
Wire Wire Line
	12650 750  12650 1650
Wire Wire Line
	12600 850  12600 2200
Wire Wire Line
	12600 2200 12700 2200
Wire Notes Line width 16 style solid rgb(0, 0, 0)
	18200 0    18200 4800
$Comp
L Device:R R_Q6
U 1 1 5F8805FE
P 12850 3850
F 0 "R_Q6" V 12950 3850 50  0000 C CNN
F 1 "R" V 12734 3850 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 3850 50  0001 C CNN
F 3 "~" H 12850 3850 50  0001 C CNN
	1    12850 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q5
U 1 1 5F880604
P 12850 3300
F 0 "R_Q5" V 12950 3300 50  0000 C CNN
F 1 "R" V 12734 3300 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 3300 50  0001 C CNN
F 3 "~" H 12850 3300 50  0001 C CNN
	1    12850 3300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q4
U 1 1 5F88060A
P 12850 2750
F 0 "R_Q4" V 12950 2750 50  0000 C CNN
F 1 "R" V 12734 2750 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 2750 50  0001 C CNN
F 3 "~" H 12850 2750 50  0001 C CNN
	1    12850 2750
	0    1    -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q4
U 1 1 5F880610
P 13400 2750
F 0 "Q4" H 13450 2750 50  0000 L CNN
F 1 "BC328" V 13600 2650 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 2675 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 2750 50  0001 L CNN
	1    13400 2750
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q5
U 1 1 5F880616
P 13400 3300
F 0 "Q5" H 13450 3300 50  0000 L CNN
F 1 "BC328" V 13600 3200 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 3225 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 3300 50  0001 L CNN
	1    13400 3300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q6
U 1 1 5F88061C
P 13400 3850
F 0 "Q6" H 13450 3850 50  0000 L CNN
F 1 "BC328" V 13600 3750 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 3775 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 3850 50  0001 L CNN
	1    13400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 2750 13200 2750
Wire Wire Line
	13500 2950 13150 2950
Connection ~ 13150 3500
Wire Wire Line
	13150 3500 13150 4050
Wire Wire Line
	13000 3850 13200 3850
Wire Wire Line
	13150 2950 13150 3500
Wire Wire Line
	13000 3300 13200 3300
Wire Wire Line
	13150 3500 13500 3500
Wire Wire Line
	13500 4050 13150 4050
Connection ~ 13150 4050
Wire Wire Line
	13150 4050 13150 4600
$Comp
L Device:R R_Q7
U 1 1 5F880632
P 12850 4400
F 0 "R_Q7" V 12950 4400 50  0000 C CNN
F 1 "R" V 12734 4400 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 12780 4400 50  0001 C CNN
F 3 "~" H 12850 4400 50  0001 C CNN
	1    12850 4400
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q7
U 1 1 5F880638
P 13400 4400
F 0 "Q7" H 13450 4400 50  0000 L CNN
F 1 "BC328" V 13600 4300 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 13600 4325 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 13400 4400 50  0001 L CNN
	1    13400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 4400 13200 4400
Wire Wire Line
	13500 4600 13150 4600
Wire Wire Line
	12550 950  12550 2750
Wire Wire Line
	12550 2750 12700 2750
Wire Wire Line
	12700 3300 12500 3300
Wire Wire Line
	12500 3300 12500 1050
Wire Wire Line
	12450 1150 12450 3850
Wire Wire Line
	12450 3850 12700 3850
Wire Wire Line
	12400 4400 12400 1250
Wire Wire Line
	12400 4400 12700 4400
Wire Wire Line
	13150 2400 13150 2950
Connection ~ 13150 2950
Wire Wire Line
	12300 1250 12400 1250
Wire Wire Line
	12300 1150 12450 1150
Wire Wire Line
	12500 1050 12300 1050
Wire Wire Line
	12300 950  12550 950 
Wire Wire Line
	12600 850  12300 850 
Wire Wire Line
	12300 750  12650 750 
Wire Wire Line
	12700 650  12300 650 
Wire Wire Line
	12300 550  12700 550 
Text GLabel 14700 4700 0    50   Input ~ 0
Vcc_LED
$Comp
L Device:R R_Q10
U 1 1 5FD9A74B
P 16000 1650
F 0 "R_Q10" V 16100 1650 50  0000 C CNN
F 1 "R" V 15884 1650 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 1650 50  0001 C CNN
F 3 "~" H 16000 1650 50  0001 C CNN
	1    16000 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q9
U 1 1 5FD9A755
P 16000 1100
F 0 "R_Q9" V 16100 1100 50  0000 C CNN
F 1 "R" V 15884 1100 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 1100 50  0001 C CNN
F 3 "~" H 16000 1100 50  0001 C CNN
	1    16000 1100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q8
U 1 1 5FD9A75F
P 16000 550
F 0 "R_Q8" V 16100 550 50  0000 C CNN
F 1 "R" V 15884 550 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 550 50  0001 C CNN
F 3 "~" H 16000 550 50  0001 C CNN
	1    16000 550 
	0    1    -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q8
U 1 1 5FD9A769
P 16550 550
F 0 "Q8" H 16600 550 50  0000 L CNN
F 1 "BC328" V 16750 450 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 475 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 550 50  0001 L CNN
	1    16550 550 
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q9
U 1 1 5FD9A773
P 16550 1100
F 0 "Q9" H 16600 1100 50  0000 L CNN
F 1 "BC328" V 16750 1000 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 1025 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 1100 50  0001 L CNN
	1    16550 1100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q10
U 1 1 5FD9A77D
P 16550 1650
F 0 "Q10" H 16600 1650 50  0000 L CNN
F 1 "BC328" V 16750 1550 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 1575 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 1650 50  0001 L CNN
	1    16550 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 CONN_R_in_8:15
U 1 1 5FD9A787
P 15250 950
F 0 "CONN_R_in_8:15" H 14750 450 50  0000 L CNN
F 1 "Conn_01x08" H 14850 350 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 15250 950 50  0001 C CNN
F 3 "~" H 15250 950 50  0001 C CNN
	1    15250 950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	15850 650  15850 1100
Wire Wire Line
	16150 550  16350 550 
Wire Wire Line
	16650 750  16300 750 
Connection ~ 16300 1300
Wire Wire Line
	16300 1300 16300 1850
Wire Wire Line
	16150 1650 16350 1650
Wire Wire Line
	15800 1650 15850 1650
Wire Wire Line
	16300 750  16300 1300
Wire Wire Line
	16150 1100 16350 1100
Wire Wire Line
	16300 1300 16650 1300
Wire Wire Line
	16650 1850 16300 1850
Connection ~ 16300 1850
Wire Wire Line
	16300 1850 16300 2400
$Comp
L Device:R R_Q11
U 1 1 5FD9A79E
P 16000 2200
F 0 "R_Q11" V 16100 2200 50  0000 C CNN
F 1 "R" V 15884 2200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 2200 50  0001 C CNN
F 3 "~" H 16000 2200 50  0001 C CNN
	1    16000 2200
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q11
U 1 1 5FD9A7A8
P 16550 2200
F 0 "Q11" H 16600 2200 50  0000 L CNN
F 1 "BC328" V 16750 2100 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 2125 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 2200 50  0001 L CNN
	1    16550 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	16150 2200 16350 2200
Wire Wire Line
	16650 2400 16300 2400
Connection ~ 16300 2400
Wire Wire Line
	15800 750  15800 1650
Wire Wire Line
	15750 850  15750 2200
Wire Wire Line
	15750 2200 15850 2200
$Comp
L Device:R R_Q14
U 1 1 5FD9A7B8
P 16000 3850
F 0 "R_Q14" V 16100 3850 50  0000 C CNN
F 1 "R" V 15884 3850 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 3850 50  0001 C CNN
F 3 "~" H 16000 3850 50  0001 C CNN
	1    16000 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q13
U 1 1 5FD9A7C2
P 16000 3300
F 0 "R_Q13" V 16100 3300 50  0000 C CNN
F 1 "R" V 15884 3300 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 3300 50  0001 C CNN
F 3 "~" H 16000 3300 50  0001 C CNN
	1    16000 3300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Q12
U 1 1 5FD9A7CC
P 16000 2750
F 0 "R_Q12" V 16100 2750 50  0000 C CNN
F 1 "R" V 15884 2750 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 2750 50  0001 C CNN
F 3 "~" H 16000 2750 50  0001 C CNN
	1    16000 2750
	0    1    -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q12
U 1 1 5FD9A7D6
P 16550 2750
F 0 "Q12" H 16600 2750 50  0000 L CNN
F 1 "BC328" V 16750 2650 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 2675 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 2750 50  0001 L CNN
	1    16550 2750
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q13
U 1 1 5FD9A7E0
P 16550 3300
F 0 "Q13" H 16600 3300 50  0000 L CNN
F 1 "BC328" V 16750 3200 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 3225 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 3300 50  0001 L CNN
	1    16550 3300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC328 Q14
U 1 1 5FD9A7EA
P 16550 3850
F 0 "Q14" H 16600 3850 50  0000 L CNN
F 1 "BC328" V 16750 3750 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 3775 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 3850 50  0001 L CNN
	1    16550 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	16150 2750 16350 2750
Wire Wire Line
	16650 2950 16300 2950
Connection ~ 16300 3500
Wire Wire Line
	16300 3500 16300 4050
Wire Wire Line
	16150 3850 16350 3850
Wire Wire Line
	16300 2950 16300 3500
Wire Wire Line
	16150 3300 16350 3300
Wire Wire Line
	16300 3500 16650 3500
Wire Wire Line
	16650 4050 16300 4050
Connection ~ 16300 4050
Wire Wire Line
	16300 4050 16300 4600
$Comp
L Device:R R_Q15
U 1 1 5FD9A7FF
P 16000 4400
F 0 "R_Q15" V 16100 4400 50  0000 C CNN
F 1 "R" V 15884 4400 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 15930 4400 50  0001 C CNN
F 3 "~" H 16000 4400 50  0001 C CNN
	1    16000 4400
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC328 Q15
U 1 1 5FD9A809
P 16550 4400
F 0 "Q15" H 16600 4400 50  0000 L CNN
F 1 "BC328" V 16750 4300 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 16750 4325 50  0001 L CIN
F 3 "http://www.redrok.com/PNP_BC327_-45V_-800mA_0.625W_Hfe100_TO-92.pdf" H 16550 4400 50  0001 L CNN
	1    16550 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	16150 4400 16350 4400
Wire Wire Line
	16650 4600 16300 4600
Wire Wire Line
	15700 950  15700 2750
Wire Wire Line
	15700 2750 15850 2750
Wire Wire Line
	15850 3300 15650 3300
Wire Wire Line
	15650 3300 15650 1050
Wire Wire Line
	15600 1150 15600 3850
Wire Wire Line
	15600 3850 15850 3850
Wire Wire Line
	15550 4400 15550 1250
Wire Wire Line
	15550 4400 15850 4400
Wire Wire Line
	16300 2400 16300 2950
Connection ~ 16300 2950
Connection ~ 16300 4600
Wire Wire Line
	15450 1250 15550 1250
Wire Wire Line
	15450 1150 15600 1150
Wire Wire Line
	15650 1050 15450 1050
Wire Wire Line
	15450 950  15700 950 
Wire Wire Line
	15750 850  15450 850 
Wire Wire Line
	15450 750  15800 750 
Wire Wire Line
	15850 650  15450 650 
Wire Wire Line
	15450 550  15850 550 
Wire Wire Line
	13500 350  13750 350 
Wire Wire Line
	13500 900  13750 900 
$Comp
L Device:R R_LED0
U 1 1 5FE603A4
P 13900 350
F 0 "R_LED0" V 13800 350 50  0000 C CNN
F 1 "R" V 13784 350 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 350 50  0001 C CNN
F 3 "~" H 13900 350 50  0001 C CNN
	1    13900 350 
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED1
U 1 1 5FE89541
P 13900 900
F 0 "R_LED1" V 13800 900 50  0000 C CNN
F 1 "R" V 13784 900 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 900 50  0001 C CNN
F 3 "~" H 13900 900 50  0001 C CNN
	1    13900 900 
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED2
U 1 1 5FEB20DF
P 13900 1450
F 0 "R_LED2" V 13800 1450 50  0000 C CNN
F 1 "R" V 13784 1450 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 1450 50  0001 C CNN
F 3 "~" H 13900 1450 50  0001 C CNN
	1    13900 1450
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED3
U 1 1 5FEB20E9
P 13900 2000
F 0 "R_LED3" V 13800 2000 50  0000 C CNN
F 1 "R" V 13784 2000 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 2000 50  0001 C CNN
F 3 "~" H 13900 2000 50  0001 C CNN
	1    13900 2000
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED4
U 1 1 5FEE3210
P 13900 2550
F 0 "R_LED4" V 13800 2550 50  0000 C CNN
F 1 "R" V 13784 2550 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 2550 50  0001 C CNN
F 3 "~" H 13900 2550 50  0001 C CNN
	1    13900 2550
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED5
U 1 1 5FEE321A
P 13900 3100
F 0 "R_LED5" V 13800 3100 50  0000 C CNN
F 1 "R" V 13784 3100 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 3100 50  0001 C CNN
F 3 "~" H 13900 3100 50  0001 C CNN
	1    13900 3100
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED6
U 1 1 5FEE3224
P 13900 3650
F 0 "R_LED6" V 13800 3650 50  0000 C CNN
F 1 "R" V 13784 3650 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 3650 50  0001 C CNN
F 3 "~" H 13900 3650 50  0001 C CNN
	1    13900 3650
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED7
U 1 1 5FEE322E
P 13900 4200
F 0 "R_LED7" V 13800 4200 50  0000 C CNN
F 1 "R" V 13784 4200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 13830 4200 50  0001 C CNN
F 3 "~" H 13900 4200 50  0001 C CNN
	1    13900 4200
	0    1    -1   0   
$EndComp
Wire Wire Line
	13500 4200 13750 4200
Wire Wire Line
	13750 3650 13500 3650
Wire Wire Line
	13500 3100 13750 3100
Wire Wire Line
	13500 2550 13750 2550
Wire Wire Line
	13500 2000 13750 2000
Wire Wire Line
	13500 1450 13750 1450
Wire Wire Line
	16650 350  16900 350 
Wire Wire Line
	16650 900  16900 900 
$Comp
L Device:R R_LED8
U 1 1 5FFF9323
P 17050 350
F 0 "R_LED8" V 16950 350 50  0000 C CNN
F 1 "R" V 16934 350 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 350 50  0001 C CNN
F 3 "~" H 17050 350 50  0001 C CNN
	1    17050 350 
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED9
U 1 1 5FFF9329
P 17050 900
F 0 "R_LED9" V 16950 900 50  0000 C CNN
F 1 "R" V 16934 900 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 900 50  0001 C CNN
F 3 "~" H 17050 900 50  0001 C CNN
	1    17050 900 
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED10
U 1 1 5FFF932F
P 17050 1450
F 0 "R_LED10" V 16950 1450 50  0000 C CNN
F 1 "R" V 16934 1450 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 1450 50  0001 C CNN
F 3 "~" H 17050 1450 50  0001 C CNN
	1    17050 1450
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED11
U 1 1 5FFF9335
P 17050 2000
F 0 "R_LED11" V 16950 2000 50  0000 C CNN
F 1 "R" V 16934 2000 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 2000 50  0001 C CNN
F 3 "~" H 17050 2000 50  0001 C CNN
	1    17050 2000
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED12
U 1 1 5FFF933B
P 17050 2550
F 0 "R_LED12" V 16950 2550 50  0000 C CNN
F 1 "R" V 16934 2550 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 2550 50  0001 C CNN
F 3 "~" H 17050 2550 50  0001 C CNN
	1    17050 2550
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED13
U 1 1 5FFF9341
P 17050 3100
F 0 "R_LED13" V 16950 3100 50  0000 C CNN
F 1 "R" V 16934 3100 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 3100 50  0001 C CNN
F 3 "~" H 17050 3100 50  0001 C CNN
	1    17050 3100
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED14
U 1 1 5FFF9347
P 17050 3650
F 0 "R_LED14" V 16950 3650 50  0000 C CNN
F 1 "R" V 16934 3650 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 3650 50  0001 C CNN
F 3 "~" H 17050 3650 50  0001 C CNN
	1    17050 3650
	0    1    -1   0   
$EndComp
$Comp
L Device:R R_LED15
U 1 1 5FFF934D
P 17050 4200
F 0 "R_LED15" V 16950 4200 50  0000 C CNN
F 1 "R" V 16934 4200 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 16980 4200 50  0001 C CNN
F 3 "~" H 17050 4200 50  0001 C CNN
	1    17050 4200
	0    1    -1   0   
$EndComp
Wire Wire Line
	16650 4200 16900 4200
Wire Wire Line
	16900 3650 16650 3650
Wire Wire Line
	16650 3100 16900 3100
Wire Wire Line
	16650 2550 16900 2550
Wire Wire Line
	16650 2000 16900 2000
Wire Wire Line
	16650 1450 16900 1450
Wire Wire Line
	14700 4700 14850 4700
Wire Wire Line
	14850 4700 14850 4600
$Comp
L Connector_Generic:Conn_01x08 CONN_out_R0:7
U 1 1 6015B0E6
P 14800 2250
F 0 "CONN_out_R0:7" H 14500 2650 50  0000 L CNN
F 1 "Conn_01x08" H 14500 2750 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 14800 2250 50  0001 C CNN
F 3 "~" H 14800 2250 50  0001 C CNN
	1    14800 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	14050 350  14500 350 
Wire Wire Line
	14500 350  14500 1950
Wire Wire Line
	14500 1950 14600 1950
Wire Wire Line
	14050 900  14400 900 
Wire Wire Line
	14400 900  14400 2050
Wire Wire Line
	14400 2050 14600 2050
Wire Wire Line
	14050 1450 14300 1450
Wire Wire Line
	14300 1450 14300 2150
Wire Wire Line
	14300 2150 14600 2150
Wire Wire Line
	14050 2000 14200 2000
Wire Wire Line
	14200 2000 14200 2250
Wire Wire Line
	14200 2250 14600 2250
Wire Wire Line
	14050 2550 14200 2550
Wire Wire Line
	14200 2550 14200 2350
Wire Wire Line
	14200 2350 14600 2350
Wire Wire Line
	14050 3100 14300 3100
Wire Wire Line
	14300 3100 14300 2450
Wire Wire Line
	14300 2450 14600 2450
Wire Wire Line
	14050 3650 14400 3650
Wire Wire Line
	14400 3650 14400 2550
Wire Wire Line
	14400 2550 14600 2550
Wire Wire Line
	14600 2650 14500 2650
Wire Wire Line
	14500 2650 14500 4200
Wire Wire Line
	14500 4200 14050 4200
$Comp
L Connector_Generic:Conn_01x08 CONN_out_R8:15
U 1 1 6031195D
P 17950 2250
F 0 "CONN_out_R8:15" H 17650 2650 50  0000 L CNN
F 1 "Conn_01x08" H 17650 2750 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B8B-EH-A_1x08_P2.50mm_Vertical" H 17950 2250 50  0001 C CNN
F 3 "~" H 17950 2250 50  0001 C CNN
	1    17950 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	17200 350  17650 350 
Wire Wire Line
	17650 350  17650 1950
Wire Wire Line
	17650 1950 17750 1950
Wire Wire Line
	17200 900  17550 900 
Wire Wire Line
	17550 900  17550 2050
Wire Wire Line
	17550 2050 17750 2050
Wire Wire Line
	17200 1450 17450 1450
Wire Wire Line
	17450 1450 17450 2150
Wire Wire Line
	17450 2150 17750 2150
Wire Wire Line
	17200 2000 17350 2000
Wire Wire Line
	17350 2000 17350 2250
Wire Wire Line
	17350 2250 17750 2250
Wire Wire Line
	17200 2550 17350 2550
Wire Wire Line
	17350 2550 17350 2350
Wire Wire Line
	17350 2350 17750 2350
Wire Wire Line
	17200 3100 17450 3100
Wire Wire Line
	17450 3100 17450 2450
Wire Wire Line
	17450 2450 17750 2450
Wire Wire Line
	17200 3650 17550 3650
Wire Wire Line
	17550 3650 17550 2550
Wire Wire Line
	17550 2550 17750 2550
Wire Wire Line
	17750 2650 17650 2650
Wire Wire Line
	17650 2650 17650 4200
Wire Wire Line
	17650 4200 17200 4200
Wire Wire Line
	14850 4600 16300 4600
Wire Wire Line
	13500 4600 14850 4600
Connection ~ 13500 4600
Connection ~ 14850 4600
Text Notes 12050 300  0    50   ~ 0
Input CTRL
Text Notes 15200 300  0    50   ~ 0
Input CTRL
Text Notes 14500 1700 0    50   ~ 0
Power Output
Text Notes 17650 1700 0    50   ~ 0
Power Output
$Comp
L Connector_Generic:Conn_01x02 CONN_POWER1
U 1 1 5E05D81A
P 14850 4200
F 0 "CONN_POWER1" V 15050 3750 50  0000 L CNN
F 1 "Conn_01x02" V 14950 3850 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 14850 4200 50  0001 C CNN
F 3 "~" H 14850 4200 50  0001 C CNN
	1    14850 4200
	0    -1   -1   0   
$EndComp
Text Notes 15200 3950 2    50   ~ 0
Power Input
Wire Wire Line
	14850 4600 14850 4400
$Comp
L Connector_Generic:Conn_01x06 CONN_PINsU0
U 1 1 5DE05DFD
P 4050 2200
F 0 "CONN_PINsU0" H 4130 2192 50  0000 L CNN
F 1 "Conn_01x06" H 4130 2101 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B6B-EH-A_1x06_P2.50mm_Vertical" H 4050 2200 50  0001 C CNN
F 3 "~" H 4050 2200 50  0001 C CNN
	1    4050 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1300 4750 2400
Text GLabel 3300 2650 0    50   Input ~ 0
PC1
Text Notes 3300 2650 0    50   ~ 0
---
Text Notes 3300 2850 0    50   ~ 0
---
Text GLabel 3300 2850 0    50   Input ~ 0
PC0
Wire Wire Line
	3300 2650 3800 2650
Wire Wire Line
	3800 2400 3850 2400
Wire Wire Line
	3800 2400 3800 2650
Wire Wire Line
	3750 2300 3850 2300
Wire Wire Line
	3650 2200 3850 2200
Wire Wire Line
	3650 2100 3850 2100
Wire Wire Line
	3750 2000 3850 2000
Wire Wire Line
	3850 2850 3850 2500
Wire Wire Line
	3300 2850 3850 2850
Wire Wire Line
	4150 3350 4750 3350
Wire Wire Line
	4050 1300 4750 1300
Wire Wire Line
	2100 3350 2550 3350
Wire Wire Line
	2100 3250 2550 3250
Text GLabel 2600 3150 2    50   Input ~ 0
SCLK
Text GLabel 2600 3450 2    50   Input ~ 0
RCLK
Wire Wire Line
	2550 3250 2550 3150
Wire Wire Line
	2550 3150 2600 3150
Connection ~ 2550 3250
Wire Wire Line
	2550 3250 2600 3250
Wire Wire Line
	2550 3350 2550 3450
Wire Wire Line
	2550 3450 2600 3450
Connection ~ 2550 3350
Wire Wire Line
	2550 3350 2600 3350
$EndSCHEMATC
