%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright (C) <2019-2020> Kevin P.Morais <moraiskv@gmail.com>

    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.

    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.

    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
printf("-- MCU Fixed-Point Test Result --\n\n")
printf("--- Fast Fourier Transform Algorithm ---\n\n")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Input Signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------
% Fixed-Point info
fp    = 8;
shamt = 2^8;

%--------------------------------------------
% Sample info
f_cpu = 20e6;       % cpu and ad converter frequency
f_ad  = f_cpu/128;

fs  = f_ad/13.5;    % sample frequency (and period)
ps  = 1/fs;

N   = 7;
S   = 2^N;          % number os samples

% input signal
A = 2.2;
f = 2e3;
w = 2*pi*f;

x = zeros(1,S);

printf("-> Input signal: \n  Amplitude = %d\n  Frequency(s) Hz = %d\n\n" , A , f );

t = 0:ps:(S*ps)-ps; % sample time vector
for i = 1:S
    x(i) = A*sin(w*t(i) + 0);
end

% ----
% convert signal in to fixed-point, and print it
for k = 1:S
    x(k) = fix(shamt*x(k));
end

printf("-> Test Data LUT:\n")
n = 8;
for k = 1:n:length(x)
    for m = 0:n-1
        printf("%d,   " , x(k+m) )
    endfor
    printf("\n")
end
printf("\n\n")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   FFT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
printf("-> FFT Info:\n")
printf("  S = %d (total of samples)\n  fs = %d (frequency sample)\n\n", S , fs )

f0 = fs/S;  % frequency resolution

y = zeros(1 , S);
y = fft_fp(x , N , fp);

% get the module (the raw output is a complex number)
yn = abs(y(1:S/2));
yn = fix(yn)*2;

printf("-> FFT Complex Output in Fixed-Point (normalized):\n")
n = 8;
for k = 1:n:S/2
    for m = 0:n-1
        printf("%d,   " , yn(k+m) )
    endfor
    printf("\n")
end
printf("\n\n")

f_vec = f0.*(1:S/2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = 1;

figure(n)
plot(t,x)
title('Signal - Input')
ylabel('Amplitude')
xlabel('Time (s)')
grid
n = n+1;


figure(n)
plot(f_vec,yn,'r')
title('FFT')
ylabel('Amplitude')
xlabel('Frequency (Hz)')
grid
n = n+1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Header Generator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
file = fopen("../src/testlut_gen.h", "w");

% -----
% start header
fputs(file, "#ifndef _TEST_LUT_GEN_H_\n#define _TEST_LUT_GEN_H_\n\n");

% -----
% write butterfly lut
fputs(file, "const int16_t gTestDataLUT[");
fputs(file, num2str(length(x)));
fputs(file, "] = {\n  ");

nAux = 0;
for k = 1:length(x)

  % convert value to str and pads with (until 5 char)
  strAux = num2str(x(k));
  while (length(strAux) < 5)
    strAux = cstrcat(strAux, " ");
  end
  fputs(file, strAux);

  % limit of 12 values at each line
  nAux++;
  if (k == length(x))
    fputs(file, "\n};\n");
  elseif (nAux >= 12)
    nAux = 0;
    fputs(file, ",\n  ");
  else
    fputs(file, ", ");
  endif
end

% end header
fputs(file, "\n#endif\n");
fclose(file);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
input("\n\n---------- Press return to exit...");
