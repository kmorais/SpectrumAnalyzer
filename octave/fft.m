%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright (C) <2019-2020> Kevin P.Morais <moraiskv@gmail.com>

    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.

    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.

    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---
%--- DIT FFT - Fast Fourier Transform
%---
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ret] = fft(x , N)
    %-----------------------
    % function inputs:
    %   x = data input vector
    %   N = number of FFT stages,
    % function outputs:
    %   x_in = output in the frequency domain, complex values not normalized
    % -----------------------
    S = 2^N;    % number of samples from 'x' to be used

    %-----------------------
    % check if the input vector has S samples, padding with 0s
    % if needed
    % note: also verify if it's a single dimension input
    [row_x , column_x] = size(x);
    if row_x > 1 && column_x > 1
        error("FFT input isn't a single dimension array")
    endif

    if row_x > 1
        rowS_x    = S;
        columnS_x = column_x;
    else
        rowS_x    = row_x;
        columnS_x = S;
    endif

    if (numel(x)) < S
        printf("\n\n  ->Warning: FFT input vector has less than %d samples, padding with 0s...\n\n" , S)
        x = resize (x , rowS_x , columnS_x);
    elseif (numel(x)) > S
        printf("\n\n  ->Warning: FFT input vector has more elements than %d samples, cutting of some...\n\n" , S)
        x = resize (x , rowS_x , columnS_x);
    endif

    clear *row*  *column*

    %-----------------------
    % butterfly diagram, separate odd and even samples to recombine it
    even_index  = zeros(1 , S/2);
    % odd_index   = zeros(1,S/2);   % odd index is just a increment (by 1) of the even one

    x_in = zeros(1 , S);

    even_index(1) = 0;
    even_index(2) = even_index(1) + S/2;

    n = 2;
    k = 3;
    while(k < S/2)  % separate odd from even
        even_index(k) = even_index(k-n)+(S/(n*2));
        for p = (k+1):(k+1)+(n-2)
            even_index(p) = even_index(p-n)+(S/(n*2));
        end
        k = k+n;
        n = 2*n;
    end

    even_index  = even_index+1;     % index correction, since octave doesn't allow (0)
    index = [ even_index (even_index+1) ];

    for k = 1:S
        x_in(k)         = x(index(k));
    end

    %-----------------------
    % create a exp LUT: exp(-2*pi*i*n)/S
    %-----------------------
    exp_lut  = zeros(1,S/2);
    temp = (-2*pi*1i)/S;
    for k = 0:(S/2)-1
        exp_lut(k+1) = exp(temp*k);
    end

    %-----------------------
    % delete some variables
    %-----------------------
    clear *index* temp S_x

    %-----------------------
    % run FFT
    %-----------------------

    x_temp = zeros(1,S);    % temporary vector
    Y = zeros(1,S/2);

    % loop control
    k = 1;
    n = 1;
    m = 1;
    stage = 0;      % current stage
    l2 = S/2;

    % --- FFT loop start
    while(stage < N)
        stage = stage +1;

        %  stage loop start
        for k = 1:2*n:S-n+1

            % gain calculation
            if n < S/2
                l = 1;
                for p = k+n:(k+n)+n-1
                    x_in(p) = x_in(p)*exp_lut(l);
                    l = l+l2;
                end
            elseif n == S/2
                l = 1;
                for p = k+n:(k+n)+n-1
                    x_in(p) = x_in(p)*exp_lut(l);
                    l = l+1;
                end
            end

            % compute even and odd output
            for p = k:k+n-1         % even
                x_temp(p) = x_in(p) + x_in(p+n);
            end
            for p = k+n:(k+n)+n-1   % odd
                x_temp(p) = x_in(p-n) - x_in(p);
            end

        end
        % stage loop end

        x_in = x_temp;
        %x_in = x_temp/2;
        n = 2*n;
        l2 = l2/2;
    end
    % --- FFT loop end

    x_in = x_in/S;

% --------------------
    ret = x_in;
endfunction
