%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright (C) <2019-2020> Kevin P.Morais <moraiskv@gmail.com>

    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.

    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.

    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

printf("--- Fast Fourier Transform Algorithm ---\n\n")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- Input Signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------
% Sample info
fs = 2^13;         % frequency sample
t = 0:1/fs:1-1/fs;  % time vector

% low frequencys
Do = 66;
Re = 74;
Mi = 83;
Fa = 88;
Sol = 99;
La = 110;
Si = 124;

% input signal
A = 0.5;
f = Si;
w = 2*pi*f;

x = zeros(1,fs);

printf("-> Input signal: \n  Amplitude = %d\n  Frequency(s) Hz = " , A );

var = 0;
for k = 1:6     % add harmonic 1 to 5
    L = 2^var;
    x = A*sin(w*L*t) + x;
    var = var + 1;
    printf("%d,  " , f*L )
end
printf("\n\n")


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- FFT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N0 = 13;
S0 = (2^N0);
f0 = fs/S0;

N1 = 7;
S1 = (2^N1);          % total of sample
f1 = fs/S1;          % FFT frequency step

printf("-> FFT Info:\n")
printf("  S = %d (total of samples)\n  fs = %d (frequency sample)\n\n", S1 , fs )

y0 = zeros(1 , S0);
y1 = zeros(1 , S1);

y0 = fft(x , N0);
y1 = fft(x , N1);

% get the module (the raw output is a complex number)
freq0= 0:S0/2-1;
y0   = abs(y0(1:S0/2));

freq1= 0:S1/2-1;
y1   = abs(y1(1:S1/2));

% divide the result in 16 columns
n = 1;
for k = 1:16
   M(k) = (y1(n)+y1(n+1)+y1(n+2)+y1(n+3));
   n = n + 4;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- Plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = 1;

figure(n)
plot(t,x)
title('Signal - Input')
ylabel('Amplitude')
xlabel('Time (s)')
grid
n = n+1;


figure(n)
plot(freq0*f0,y0,'r')
title('FFT : S = fs')
ylabel('Amplitude')
xlabel('Frequency (Hz)')
grid
n = n+1;


figure(n)
plot(freq1*f1,y1,'r')
title('FFT : S = 128')
ylabel('Amplitude')
xlabel('Frequency (Hz)')
grid
n = n+1;


figure(n)
plot(1:16,M,'o-')
title('FFT - Output to 16 Columns')
xlabel('Column')
ylabel('Row')
grid
n = n+1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
input("\n\n---------- Press return to exit...");
