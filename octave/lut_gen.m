%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright (C) <2019-2020> Kevin P.Morais <moraiskv@gmail.com>

    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.

    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.

    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% -- Script to generate LUT (Look-Up-Table) for the Butterfly Diagram and
% -- the Exponential Vector, presents at the FFT Algorithm
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

S   = 128;    % total of samples to create the LUT's
S_h = S/2;

fixed_point = 8;
shamt = 2^(fixed_point);

printf("\n----------------------------------------------------------------");
printf("\n-- LUT Generator for S = %d (samples) and fixed-point I:%d\n", S , fixed_point);
printf("----------------------------------------------------------------\n\n");

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Butterfly LUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
printf("\n\n  -> Butterfly Diagram LUT:\n")
even_index  = zeros(1 , S_h);
% odd_index   = zeros(1,S_h);   % odd index is just a increment (by 1) of the even one

even_index(1) = 0;
even_index(2) = even_index(1) + S_h;

k = 3;
m = 2;

while(k < S_h)  % separate odd from even
  even_index(k) = even_index(k-m)+(S/(m*2));
  for n = (k+1):(k+1)+(m-2)
    even_index(n) = even_index(n-m)+(S/(m*2));
  end
  k = k+m;
  m = 2*m;
end

butterfly_lut   = [ even_index (even_index+1) ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Exponential LUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --------------------
% exp LUT: exp(-2*pi*i*n)/S
% note: since values above S_h are just mirrored, they are ignored
S_h = S_h;
exp_lut = zeros(1, S_h);
temp    = (-2*pi*1i)/S;

for k = 0:S_h-1
  exp_lut(k+1) = exp(temp*k);
end

printf("\n\n  -> Exponential Real LUT:\n")
realLUT = zeros(1, S_h);

for k = 1:length(exp_lut)
  % realLUT(k) = real(exp_lut(k) * (2^8));     % raw
  realLUT(k) = fix( real(exp_lut(k)) *shamt ); % fixed pointe adjusted
end

printf("\n\n  -> Exponential Imaginary LUT:\n")
imagLUT = zeros(1, S_h);
for k = 1:length(exp_lut)
  % imagLUT(k) = imag(exp_lut(k));             % raw
  imagLUT(k) = fix( imag(exp_lut(k)) *shamt ); % fixed point adjusted
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Header Generator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
file = fopen("../src/lut_gen.h", "w");

% -----
% start header
fputs(file, "#ifndef _LUT_GEN_H_\n#define _LUT_GEN_H_\n\n");

% -----
% write butterfly lut
fputs(file, "const uint8_t gBufferflyLUT[");
fputs(file, num2str(length(butterfly_lut)));
fputs(file, "] = {\n  ");

nAux = 0;
for k = 1:length(butterfly_lut)

  % convert value to str and pads with (until 5 char)
  strAux = num2str(butterfly_lut(k));
  while (length(strAux) < 5)
    strAux = cstrcat(strAux, " ");
  end
  fputs(file, strAux);

  % limit of 12 values at each line
  nAux++;
  if (k == length(butterfly_lut))
    fputs(file, "\n};\n");
  elseif (nAux >= 12)
    nAux = 0;
    fputs(file, ",\n  ");
  else
    fputs(file, ", ");
  endif
end

% -----
% write exponential real lut
fputs(file, "const int16_t gRealExpLUT[");
fputs(file, num2str(length(realLUT)));
fputs(file, "] = {\n  ");

nAux = 0;
for k = 1:length(realLUT)

  % convert value to str and pads with (until 5 char)
  strAux = num2str(realLUT(k));
  while (length(strAux) < 5)
    strAux = cstrcat(strAux, " ");
  end
  fputs(file, strAux);

  % limit of 12 values at each line
  nAux++;
  if (k == length(realLUT))
    fputs(file, "\n};\n");
  elseif (nAux >= 12)
    nAux = 0;
    fputs(file, ",\n  ");
  else
    fputs(file, ", ");
  endif
end

% -----
% write exponential imag lut
fputs(file, "const int16_t gImagExpLUT[");
fputs(file, num2str(length(imagLUT)));
fputs(file, "] = {\n  ");

nAux = 0;
for k = 1:length(imagLUT)

  % convert value to str and pads with (until 5 char)
  strAux = num2str(imagLUT(k));
  while (length(strAux) < 5)
    strAux = cstrcat(strAux, " ");
  end
  fputs(file, strAux);

  % limit of 12 values at each line
  nAux++;
  if (k == length(imagLUT))
    fputs(file, "\n};\n");
  elseif (nAux >= 12)
    nAux = 0;
    fputs(file, ",\n  ");
  else
    fputs(file, ", ");
  endif
end

% end header
fputs(file, "\n#endif\n");
fclose(file);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --------------------
% input("---------- Press return to exit...");
